<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 25.02.17
 * Time: 15:07
 */

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="libs/jquery/jquery-1.11.2.min.js"></script>
    <title><?= wp_get_document_title('|', true, 'right'); ?></title>
    <?php wp_head(); ?>
</head>
<body>
<!-- Здесь пишем код -->
<header>
    <div class="wrapper">
        <div class="row">
            <a href="<?= get_home_url(); ?>" class="logo">
                <img src="<?= get_option('logo') ?>" alt="<?= get_bloginfo('name') ?>">
            </a>
            <?php wp_nav_menu(array(
                'theme_location' => '',
                'menu' => 2,
                'container' => false,
                'container_class' => '',
                'container_id' => '',
                'menu_class' => '',
                'menu_id' => '',
                'echo' => true,
                'fallback_cb' => 'wp_page_menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul id="%1$s" class="header-menu mobile-hidden">%3$s</ul>',
                'depth' => 0,
                'walker' => '',
            )); ?>
            <div class="header-contacts mobile-hidden">
                <ul class="phone-list">
                    <?php if (get_option('phone1')): ?>
                        <li><?= get_option('phone1') ?></li>
                    <?php endif; ?>
                    <?php if (get_option('phone2')): ?>
                        <li><?= get_option('phone2') ?></li>
                    <?php endif; ?>
                </ul>
                <a href="#invite-to-course" class="invite-btn">записаться на курс</a>
            </div>
            <button class="mobile-visible" id="mobile-menu-btn">меню <i class="fa fa-bars" aria-hidden="true"></i></button>

        </div>
    </div>
</header>
