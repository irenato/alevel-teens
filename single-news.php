<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 12:35
 */

get_header();

?>

    <section class="top-block-news-page mobile-hidden">
        <div class="wrapper">
            <img src="img/news-baner.png" class="main-news-image" alt="A-level">
            <h1 class="main-news-title">Корпоратив по мотивам Алисы в стране чудес</h1>
            <p class="main-news-date">30.12.16</p>
            <p class="main-news-description">
                Что будет, если пройти сквозь кроличью нору? Да-да, все наверняка помнят
                ту странную историю с Алисой. Вот на нашей новогодней вечеринке
                мы и решили это проверить. И чудеса не заставили себя ждать. Не верите?
                Смотрите фото
            </p>
            <button>читать новость</button>
        </div>
    </section>

    <section class="news">
        <div class="wrapper mobile-hidden">
            <h2 class="double-title">новости</h2>
            <div class="carusel-news" id="carusel_news_desc">
                <div class="news-stack">
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                </div>
                <div class="news-stack">
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper mobile-visible">
            <div class="carusel-news" id="carusel_news_mob">
                <div class="news-stack">
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                </div>
                <div class="news-stack">
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                </div>
                <div class="news-stack">
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                </div>
                <div class="news-stack">
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php

get_footer();
