<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 25.02.17
 * Time: 15:07
 */

?>

<footer id="contacts">
    <div class="wrapper mobile-hidden">
        <div class="row-map">
            <div class="map-contacts">
                <h2>наши контакты</h2>
                <p class="email">
                    <i class="ic-email-outline"></i>
                    <span><?= get_option('admin_email') ?></span>
                </p>
                <p class="phones">
                    <i class="ic-phone"></i>
                    <?php if (get_option('phone1')): ?>
                        <span><?= get_option('phone1') ?></span>
                    <?php endif; ?>
                    <?php if (get_option('phone2')): ?>
                        <span><?= get_option('phone2') ?></span>
                    <?php endif; ?>
                </p>
                <button id="contact-button" class="callback-btn">заказать звонок</button>
                <p class="street">
                    <i class="ic-map-marker"></i>
                    <span><?= get_option('address') ?></span>
                </p>
                <p class="social-network-links">
                    <?php if (get_option('vkontakte_url')): ?>
                        <a href="<?= get_option('vkontakte_url') ?>" target="_blank" rel="nofollow"><i
                                class="ic-vk"></i></a>
                    <?php endif; ?>
                    <?php if (get_option('googleplus_url')): ?>
                        <a href="<?= get_option('googleplus_url') ?>"><i class="fa fa-google-plus" aria-hidden="true"
                                                                         target="_blank" rel="nofollow"></i></a>
                    <?php endif; ?>
                    <?php if (get_option('facebook_url')): ?>
                        <a href="<?= get_option('facebook_url') ?>"><i class="ic-facebook" target="_blank"
                                                                       rel="nofollow"></i></a>
                    <?php endif; ?>
                </p>
            </div>
        </div>
        <div class="row-bottom-menu">
            <div class="col-about-school">
                <h3>A-level ukraine</h3>
                <?php wp_nav_menu(array(
                    'theme_location' => '',
                    'menu' => 3,
                    'container' => false,
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => '',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth' => 0,
                    'walker' => '',
                )); ?>
            </div>
            <div class="col-courses">
                <h3>Курсы</h3>
                <ul>
                    <?php $args = array(
                        'offset' => 0,
                        'post_type' => 'courses',
                        'posts_per_page' => -1); ?>
                    <?php $post_courses = new WP_query($args); ?>
                    <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
                        <li><a href="<?php the_permalink(); ?>"><?= get_the_title() ?></a></li>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                </ul>
            </div>
            <div class="col-contacts">
                <h3>Контакты</h3>
                <p class="phones">
                    <i class="ic-phone"></i>
                    <?php if (get_option('phone1')): ?>
                        <span><?= get_option('phone1') ?></span>
                    <?php endif; ?>
                    <?php if (get_option('phone2')): ?>
                        <span><?= get_option('phone2') ?></span>
                    <?php endif; ?>
                </p>
                <p class="email">
                    <i class="ic-telegram"></i>
                    <span><?= get_option('admin_email') ?></span>
                </p>
                <p class="street">
                    <i class="ic-map-marker"></i>
                    <span><?= get_option('city') ?></span>
                    <span><?= get_option('address') ?></span>
                </p>
            </div>
        </div>
        <div class="row-subscription">
            <p class="copyright">Copyrights © 2016. All Rights Reserved.</p>
        </div>
    </div>
    <div class="wrapper mobile-visible">
        <h3>наши контакты: </h3>
        <p>
            <?php if (get_option('phone1')): ?>
                <span><?= get_option('phone1') ?></span>
            <?php endif; ?>
            <?php if (get_option('phone2')): ?>
                <span><?= get_option('phone2') ?></span>
            <?php endif; ?>
        </p>
        <p>
            <span><?= get_option('admin_email') ?></span>
            <span><?= get_option('address') ?></span>
        </p>
        <p>
            <?php if (get_option('vkontakte_url')): ?>
                <a href="<?= get_option('vkontakte_url') ?>" target="_blank" rel="nofollow"><i
                        class="ic-vk"></i></a>
            <?php endif; ?>
            <?php if (get_option('googleplus_url')): ?>
                <a href="<?= get_option('googleplus_url') ?>"><i class="fa fa-google-plus" aria-hidden="true"
                                                                 target="_blank" rel="nofollow"></i></a>
            <?php endif; ?>
            <?php if (get_option('facebook_url')): ?>
                <a href="<?= get_option('facebook_url') ?>"><i class="ic-facebook" target="_blank"
                                                               rel="nofollow"></i></a>
            <?php endif; ?>
        </p>
    </div>
</footer>

<div class="hidden">
    <?php get_template_part('modals/review'); ?>
    <?php get_template_part('modals/for_teachers'); ?>
    <?php get_template_part('modals/comfirmation'); ?>
    <?php get_template_part('modals/about_course'); ?>
    <?php get_template_part('modals/mobile_menu'); ?>
    <?php get_template_part('modals/contact'); ?>
</div>

<?php wp_footer(); ?>
</body>
</html>
