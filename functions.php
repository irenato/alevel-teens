<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 25.02.17
 * Time: 15:03
 */

require 'settings/included_scripts.php';
require 'settings/custom_posts.php';
require 'settings/theme_settings.php';
require 'settings/month.php';
require 'settings/ajax.php';
require 'settings/admin.php';
require 'settings/admin_students.php';
require 'settings/admin_teachers.php';
require 'settings/hidden.php';


add_theme_support('post-thumbnails');

function themeslug_theme_customizer($wp_customize)
{
    $wp_customize->add_section('themeslug_logo_section', array(
        'title' => __('Logo', 'themeslug'),
        'priority' => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));

    $wp_customize->add_setting('themeslug_logo');
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', array(
        'label' => __('Logo', 'themeslug'),
        'section' => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    )));
}

add_action('customize_register', 'themeslug_theme_customizer');