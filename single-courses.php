<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 12:28
 */


get_header();

if (have_posts()) : while (have_posts()) :
    the_post();
    $post_id = $post->ID;
    ?>

    <section class="top-block-course-page">
        <div class="wrapper"
             style="background: url(<?= get_the_post_thumbnail_url($post->ID, 'full') ?>)no-repeat center center;">
            <h1 class="mobile-hidden">
                <?= get_option('alevel_pagetitle'); ?> <br/>
                <span><?= get_option('alevel_pagetitle_description') ?></span>
            </h1>
            <ul class="mobile-hidden">
                <li class="time">
                    <img src="<?= get_template_directory_uri() ?>/img/clock.png" alt="A-Level">
                    <p><?= get_field('course_day') ?></p>
                    <p><?= get_field('course_time') ?></p>
                </li>
                <!--                <li class="date">-->
                <!--                    <img src="--><? //= get_template_directory_uri()
                ?><!--/img/calendar.png" alt="A-Level">-->
                <!--                    <p>--><? //= get_field('course_duration')
                ?><!--</p>-->
                <!--                    <p>--><? //= createDate(get_field('course_start'))
                ?><!-- - --><? //= createDate(get_field('course_finish'))
                ?><!--</p>-->
                <!--                </li>-->
                <li class="cost">
                    <img src="<?= get_template_directory_uri() ?>/img/money.png" alt="A-Level">
                    <p><?= get_field('course_coast') ?></p>
                    <!--                    <p>за весь курс</p>-->
                </li>
                <li class="free-places">
                    <img src="<?= get_template_directory_uri() ?>/img/finish.png" alt="A-Level">
                    <p>свободных мест</p>
                    <p>
                        <?= get_field('course_free_places') ?>
                        из
                        <?= get_field('course_places') ?>
                    </p>
                </li>
            </ul>
        </div>
        <div class="wrapper mobile-visible">
            <ul>
                <li>
                    <img src="<?= get_template_directory_uri() ?>/img/money.png" alt="A-Level">
                    <p>
                        <span><?= get_field('course_coast') ?></span>
                    </p>
                </li>
                <li>
                    <img src="<?= get_template_directory_uri() ?>/img/finish.png" alt="A-Level">
                    <p>
                        <span><?= get_field('course_free_places') ?>/<?= get_field('course_places') ?></span>
                    </p>
                </li>
            </ul>
            <a href="#invite-to-course">записаться на курс</a>
        </div>
    </section>

    <section class="description-of-course">
        <div class="wrapper">
            <h2 class="double-title mobile-hidden"><?= get_the_title() ?></h2>
            <h2 class="double-title mobile-visible">о курсe</h2>
            <div class="row">
                <div class="mobile-hidden">
                    Сложность:
                    <ul>
                        <?php for ($i = 1; $i <= 5; $i++) : ?>
                            <li class="hexagon <?= $i <= (int)get_field('course_level') ? 'full' : '' ?>"></li>
                        <?php endfor; ?>
                    </ul>
                </div>
                <div>
                    <p>
                        <?= get_the_content() ?>
                    </p>
                    <a class="invite-btn mobile-hidden" href="#invite-to-course">записаться на курс</a>
                </div>
            </div>

        </div>
    </section>
    <?php $connected = new WP_Query(array(
    'connected_type' => 'courses_to_teachers',
    'connected_items' => get_queried_object(),
    'nopaging' => true,)); ?>
    <?php if ($connected->have_posts()) : ?>
    <section class="teachers-teens">
        <div class="wrapper">

            <h2 class="double-title">преподаватели курса</h2>

            <div class="row">

                <?php while ($connected->have_posts()) : $connected->the_post(); ?>
                    <div class="teacher">
                        <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?= get_the_title() ?>">
                        <h3><?= get_the_title() ?></h3>
                        <p><?= get_the_content() ?></p>
                    </div>
                <?php endwhile; ?>

            </div>
        </div>
    </section>
    <?php endif; ?>

    <?php $course_programs = get_field('course_program', $post_id); ?>
    <?php if ($course_programs): ?>
    <section class="program">
        <div class="wrapper">
            <h2>программа курса</h2>
            <ul>
                <?php foreach ($course_programs as $course_program): ?>
                    <li class="modules">
                        <div class="left">
                            <p><?= $course_program['course_period'] ?></p>
                            <img src="<?= $course_program['course_period_image'] ?>" alt="A-level">
                        </div>
                        <div class="right">
                            <h3>Практика:</h3>
                            <ul>
                                <?php foreach ($course_program['course_practic_block'] as $item) : ?>
                                    <li>
                                        <img src="<?= get_template_directory_uri() ?>/img/check.svg"
                                             alt="check A-level">
                                        <?= $item['course_practic_block_item'] ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <h3>Теория:</h3>
                            <ul>
                                <?php foreach ($course_program['course_theory_block'] as $item): ?>
                                    <li>
                                        <img src="<?= get_template_directory_uri() ?>/img/check.svg"
                                             alt="check A-level">
                                        <?= $item['course_theory_block_item'] ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <button class="course-show-more"
                                    data-title="Вся теория за <?= $course_program['course_period'] ?>">
                                Вся теория за <?= $course_program['course_period'] ?>
                            </button>
                        </div>
                        <div class="full-description hidden">
                            <ol>
                                <?php foreach ($course_program['course_all_theory_block'] as $item): ?>
                                    <li><?= $item['course_theory_block_item'] ?></li>
                                <?php endforeach; ?>
                            </ol>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>
<?php endif; ?>

    <?php get_template_part('template-parts/form_course'); ?>

<?php endwhile; ?>
<?php endif; ?>
<?php

get_footer();
