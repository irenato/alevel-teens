<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 12:06
 */

?>
<div class="thanks-popup">
    <div class="wrapper">
        <div class="con">
            <img src="<?= get_template_directory_uri()  ?>/img/thnks-img.png" alt="A-level">
            <p>спасибо</p>
            <p>ваша заявка принята, ожидайте ответ</p>
            <button id="close-thanks-popup">вернуться на сайт</button>
        </div>
    </div>
</div>
