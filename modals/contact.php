<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 08.03.17
 * Time: 0:47
 */

?>

<div class="contact-popup">
    <div class="wrapper">
        <form action="" method="post" class="header-form">
            <div class="input-container">
                <input type="text" name="name" placeholder="ИМЯ*">
                <p class="error-message">Введите имя</p>
            </div>
            <div class="input-container">
                <input type="text" name="phone" placeholder="ТЕЛЕФОН*">
                <p class="error-message">Введите телефон</p>
            </div>
            <div class="input-container">
                <input type="email" name="email" placeholder="E-MAIL*">
                <p class="error-message">Введите email</p>
            </div>
            <input type="hidden" name="course" value="1">
            <div class="input-container">
                <button class="submit-btn" type="submit">заказать звонок</button>
            </div>
        </form>
    </div>
</div>
