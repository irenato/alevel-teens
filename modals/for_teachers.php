<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 12:07
 */

?>

<div class="teachers-popup">
    <div class="wrapper">
        <form action="" class="invite-to-course-m form-for-teachers">
            <div class="inputs">
                <div class="input-container">
                    <input type="text" name="first-name" placeholder="Имя">
                    <p class="error-message">Введите имя</p>
                </div>
                <div class="input-container">
                    <input type="text" name="last-name" placeholder="Фамилия">
                    <p class="error-message">Введите фамилию</p>
                </div>
                <div class="input-container">
                    <input type="text" name="phone" placeholder="Телефон">
                    <p class="error-message">Введите телефон</p>
                </div>
                <div class="input-container">
                    <input type="text" name="email" placeholder="E-Mail">
                    <p class="error-message">Введите email</p>
                </div>
            </div>
            <div class="input-container">
                <input type="text" name="course" placeholder="Курс">
                <p class="error-message">Введите курс</p>
            </div>
            <button type="submit" class="submit-invite-form">ЗАПИСАТЬСЯ</button>
            <p>
                *Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае
                не будут переданы другим пользователям и никогда не будут открыты для просмотра.
                Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.
            </p>
        </form>
    </div>
</div>

