<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 12:07
 */

?>

<div class="mobile-main-menu">
    <div class="wrapper">
        <?php
        $menu_classes = array('fa-home', 'fa-graduation-cap', 'fa-users', 'fa-commenting', 'fa-map-marker');
        $i = -1;
        $menu_items = wp_get_nav_menu_items(4);
        if ($menu_items) {
            $menu_list = '<ul>';
            foreach ((array)$menu_items as $key => $menu_item) {
                ++$i;
                $menu_list .= '<li><a href="' . $menu_item->url . '"><i class="fa ' . $menu_classes[$i] . '"> ' . $menu_item->title . '</i></a></li>';
            }
            $menu_list .= '</ul>';
        }
        ?>
        <?= $menu_list ?>
    </div>
</div>
