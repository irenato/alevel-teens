<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 12:08
 */

?>

<div class="comment-popup">
        <div class="wrapper">
            <form action="" method="post" class="invite-to-course-m add-review" enctype="multipart/form-data">
                <div class="inputs">
                    <div class="input-container">
                        <input type="text" name="first-name" placeholder="Имя">
                        <p class="error-message">Введите имя</p>
                    </div>
                    <div class="input-container">
                        <input type="text" name="last-name" placeholder="Фамилия">
                        <p class="error-message">Введите фамилию</p>
                    </div>
                    <div class="input-container">
                        <input type="text" name="email" placeholder="E-Mail">
                        <p class="error-message">Введите email</p>
                    </div>
                    <div class="input-container">
                        <select name="course">
                            <?php $args = array(
                                'offset' => 0,
                                'post_type' => 'courses',
                                'posts_per_page' => -1); ?>
                            <?php $post_courses = new WP_query($args); ?>
                            <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>

                                <option value="<?= get_the_title() ?>"><?= get_the_title() ?></option>

                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                        </select>
                        <p class="error-message">Выберете курс</p>
                    </div>
                </div>
                <textarea name="comment" cols="30" rows="10"></textarea>
                <div class="popup-comments-btns">
                    <label for="add_photo"><i class="fa fa-camera" aria-hidden="true"></i></label>
                    <input type="file" name="photo" style="display: none" id="add_photo" accept="image/jpeg,image/png">
                    <button type="submit" class="submit-invite-form">Отправить</button>
                </div>
                <p>
                    *Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае
                    не будут переданы другим пользователям и никогда не будут открыты для просмотра.
                    Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.
                </p>
            </form>
        </div>
    </div>
</div>

