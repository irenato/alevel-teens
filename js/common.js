jQuery(document).ready(function($){
	    $('.carusel').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }
            }
        ]
    });
    $('#carusel_news_desc').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    $('#carusel_news_mob').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    
    $('.modules').on('click','.course-show-more', function(e){
    	$($(e.currentTarget.parentElement).next('div')).toggleClass('hidden');
    });

    $('#mobile-menu-btn').on('click',function () {
        $('.mobile-main-menu').show();
    });
    $('.mobile-main-menu').on('click',function () {
        $('.mobile-main-menu').hide();
    });

    $('#add_comment').on('click',function () {
        $('.comment-popup').show();
    });   
    $('.comment-popup').on('click', function () {
        $('.comment-popup').hide();
    });
    $('#invite_teacher').on('click',function () {
        $('.teachers-popup').show();
    });
    $('.invite-to-course-m').on('click', function (e) {
        e.stopPropagation();
    });
    $('.teachers-popup').on('click', function () {
        $('.teachers-popup').hide();
    });
    $('#close-thanks-popup').on('click', function () {
        $('.thanks-popup').hide();
    });

});