/**
 * Created by renato on 04.03.17.
 */

jQuery(document).ready(function ($) {
    $('body').on('click', 'a.confirm-it-now', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'confirmApplicationFromStudents',
                'id': $(this).attr('data-id'),
            },
            success: location.reload(),
        });
    });

    $('body').on('click', 'a.delete-now', function (e) {
        e.preventDefault();
        if (confirm("Вы уверены?"))
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'deleteApplicationFromStudents',
                    'id': $(this).attr('data-id'),
                },
                success: location.reload(),
            });
    });

    $('body').on('click', 'a.confirm-teacher', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'confirmApplicationFromTeachers',
                'id': $(this).attr('data-id'),
            },
            success: location.reload(),
        });
    });

    $('body').on('click', 'a.remove-teacher', function (e) {
        e.preventDefault();
        if (confirm("Вы уверены?"))
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'deleteApplicationFromTeachers',
                    'id': $(this).attr('data-id'),
                },
                success: location.reload(),
            });
    });
})