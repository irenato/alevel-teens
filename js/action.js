/**
 * Created by renato on 25.02.17.
 */

jQuery(document).ready(function ($) {
    $("a[href^='#']").on("click", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 2000);
    });

    $("input[name=phone]").mask("+38?(999) 999-99-99");

    $('form.header-form, form.invite-to-course').on('submit', function () {
        var current_form = $(this);
        $(this).find('input').each(function () {
            var name = $(this).attr('name');
            switch (name) {
                case 'name':
                    checkName(this);
                    break;

                case 'phone':
                    checkPhone(this);
                    break;

                case 'email':
                    checkEmail(this);
                    break;
            }
        })
        if ($(current_form).find('.error').length < 1) {
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'inviteCourse',
                    'user': $(current_form).serialize()
                },
                success: checkResult,
            })
        }
        return false;
    })

    $('form.form-for-teachers').on('submit', function () {
        var current_form = $(this);
        $(this).find('input').each(function () {
            var name = $(this).attr('name');
            switch (name) {

                case 'first-name':
                    checkName(this);
                    break;

                case 'last-name':
                    checkName(this);
                    break;

                case 'phone':
                    checkPhone(this);
                    break;

                case 'email':
                    checkEmail(this);
                    break;
            }
        })
        if ($(current_form).find('.error').length < 1) {
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'newTeacher',
                    'user': $(current_form).serialize()
                },
                success: checkResult,
            })
        }
        return false;
    })


    $('form.add-review').on('submit', function () {
        var current_form = $(this);
        $(this).find('input').each(function () {
            var name = $(this).attr('name');
            switch (name) {

                case 'first-name':
                    checkName(this);
                    break;

                case 'last-name':
                    checkName(this);
                    break;

                case 'email':
                    checkEmail(this);
                    break;
            }
        })
        if ($(current_form).find('.error').length < 1) {

            var formData = new FormData(),
                review_files = $('#add_photo')[0].files;
            formData.append('action', 'addReview');
            $.each(review_files, function (key, value) {
                var ext = value.name.split(".");
                if (ext[ext.length - 1] != 'jpg' && ext[ext.length - 1] != 'pdf' && ext[ext.length - 1] != 'png') {
                    alert("Формат файла(" + ext[ext.length - 1] + ") не поддерживается!");
                    return;
                }
                if (value.size > 3000000) {
                    alert("Превышен максимальный размер файла - 3МБ!");
                    return;
                }
                formData.append(key, value);
            });
            formData.append('review', $(current_form).serialize());
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                contentType: false,
                processData: false,
                data: formData,
                success: checkResult,
            })
        }
        return false;
    })

    function checkResult(result) {
        if (result) {
            $(document).find('input').val('');
            $('.teachers-popup').hide();
            $('.comment-popup').hide();
            $('.contact-popup').hide();
            $('.thanks-popup').show();
        }
    }

    function checkEmail(that) {
        var rv_mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if ($(that).val() !== '' && rv_mail.test($(that).val())) {
            $(that).removeClass('error');
            return true;
        } else {
            $(that).addClass('error');
            return false;
        }
    }

    function checkPhone(that) {
        var rv_name = /^\+38\(0[0-9]{2}\) [0-9]{3}\-[0-9]{2}\-[0-9]{2}?_?$/;

        if (rv_name.test($(that).val())) {
            $(that).removeClass('error');
            return true;
        } else {
            $(that).addClass('error');
            return false;
        }
    }

    function checkName(that) {
        var rv_name = /[a-zA-Zа-яА-я]+(\W+)?(\s+)?/;
        if ($(that).val().length > 2 && $(that).val() !== '' && rv_name.test($(that).val())) {
            $(that).removeClass('error');
            return true;
        } else {
            $(that).addClass('error');
            return false;
        }
    }

    $('#contact-button').on('click',function () {
        $('.contact-popup').show();
    });

    $('.contact-popup .header-form').on('click', function (e) {
        e.stopPropagation();
    });

    $('.contact-popup').on('click', function () {
        $('.contact-popup').hide();
    });

});
