<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 11:06
 */
/**
 * Template name: About-us page
 */

get_header();

?>

<?php while (have_posts()) : the_post(); ?>

    <section class="top-block-aboutUs-page mobile-hidden"
             style=" background-image: url(<?= get_the_post_thumbnail_url($post->ID, 'full') ?>) no-repeat center center;">
        <div class="wrapper">
            <h1>
                <?= get_option('alevel_pagetitle'); ?> <br/>
                <span><?= get_option('alevel_pagetitle_description') ?></span>
            </h1>
        </div>
    </section>

    <section class="top-block-aboutUS-mobile mobile-visible">
        <div class="banner-mob" style="background-image: url(<?= get_the_post_thumbnail_url($post->ID, 'full') ?>)"></div>
        <div class="wrapper">
            <h3>
                <?= get_the_content() ?>
            </h3>
        </div>
    </section>

    <section class="about-school mobile-hidden">
        <div class="wrapper">
            <h2 class="double-title"><?= get_the_title() ?></h2>
            <div class="row">
                <img src="<?= get_field('about_image') ?>" alt="<?= get_the_title() ?>">
                <div class="about-school-text">
                    <?= get_the_content() ?>
                </div>
            </div>
        </div>
    </section>


    <section class="all-for-happy">
        <div class="wrapper">
            <h2 class="double-title ">все для успеха</h2>
            <ul>
                <?php $steps = get_field('steps'); ?>
                <?php foreach ($steps as $step): ?>
                    <li>
                        <img src="<?= $step['image'] ?>" alt="<?= $step['title'] ?>">
                        <h3><?= $step['title'] ?></h3>
                        <p>
                            <?= $step['description'] ?>
                        </p>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>

<?php endwhile; ?>


<?php get_template_part('template-parts/form_course'); ?>

<?php get_template_part('template-parts/reviews_block'); ?>


<?php

get_footer();
