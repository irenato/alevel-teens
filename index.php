<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 25.02.17
 * Time: 15:03
 */

get_header(); ?>

    <!--START CONTENT-->
<?php
//check for main page
if ( is_front_page()  ) {
    // Include the featured content template.
    get_template_part( 'home' );
}else if(is_single()){
    get_template_part( 'single' );
}


?>


    <!--END CONTENT-->

<?php get_footer(); ?>