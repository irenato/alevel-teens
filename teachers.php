<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 12:33
 */

/**
 * Template name: Teachers page
 */

get_header();

?>

    <section class="top-block-teachers-page">
        <div class="wrapper">
            <h1>
                <?= get_option('alevel_pagetitle'); ?> <br/>
                <span><?= get_option('alevel_pagetitle_description') ?></span>
            </h1>
        </div>
    </section>

    <section class="teachers-teens">
        <div class="wrapper">

            <h2 class="double-title">наши преподы</h2>
            <div class="row">

                <?php $args = array(
                    'offset' => 0,
                    'post_type' => 'teachers',
                    'posts_per_page' => -1); ?>
                <?php $post_teachers = new WP_query($args); ?>
                <?php while ($post_teachers->have_posts()) : $post_teachers->the_post(); ?>

                    <div class="teacher">
                        <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?= get_the_title() ?>">
                        <h3><?= get_the_title() ?></h3>
                        <p><?= get_the_content() ?></p>
                    </div>

                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

            </div>

            <div class="teachers-btns">
                <button id="invite_teacher">стать преподом</button>
            </div>
        </div>
    </section>

<?php get_template_part('template-parts/form_course'); ?>

<?php

get_footer();



