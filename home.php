<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 25.02.17
 * Time: 15:05
 */
get_header();

?>

    <section class="top-block"
             style=" background: url(<?= get_the_post_thumbnail_url($post->ID, 'full') ?>) no-repeat center center;">
        <div class="wrapper mobile-hidden">
            <div class="form-invite">
                <h1>
                    <?= get_option('alevel_pagetitle'); ?> <br/>
                    <span><?= get_option('alevel_pagetitle_description') ?></span>
                </h1>
                <form action="" method="post" class="header-form">
                    <div class="input-container">
                        <input type="text" name="name" placeholder="ИМЯ*">
                        <p class="error-message">Введите имя</p>
                    </div>
                    <div class="input-container">
                        <input type="text" name="phone" placeholder="ТЕЛЕФОН*">
                        <p class="error-message">Введите телефон</p>
                    </div>
                    <div class="input-container">
                        <input type="email" name="email" placeholder="E-MAIL*">
                        <p class="error-message">Введите email</p>
                    </div>
                    <div class="input-container">
                        <button class="submit-btn" type="submit">записаться</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="wrapper mobile-visible">
            <h1>
                <?= get_option('alevel_pagetitle'); ?> <br/>
                <span><?= get_option('alevel_pagetitle_description') ?></span>
            </h1>
            <a href="#invite-to-course">записаться</a>
        </div>
    </section>

    <section class="block-description">
        <div class="wrapper">
            <?php while (have_posts()) : the_post(); ?>
                <h3 class="mobile-hidden"><?= get_the_title() ?></h3>
                <div class="row">
                    <p>
                        <?= get_the_content() ?>
                    </p>
                    <img src="<?= get_field('about_image') ?>" alt="A-Level teens">
                </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php $about_page = get_post(5) ?>
            <a href="<?= $about_page->guid ?>" class="mobile-hidden">подробнее</a>
        </div>
    </section>

    <section class="sec-courses-kids" id="courses">
        <div class="wrapper">
            <div class="row">
                <h2 class="double-title">идет набор</h2>
                <div class="courses">
                    <?php $args = array(
                        'offset' => 0,
                        'post_type' => 'courses',
                        'posts_per_page' => -1); ?>
                    <?php $post_courses = new WP_query($args); ?>
                    <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
                        <div class="courses-item">
                            <p class="places">Осталось мест:
                                <span>
                                    <?= get_field('course_free_places') ?>
                                    из
                                    <?= get_field('course_places') ?>
                                </span>
                            </p>
                            <img src="<?= get_field('logo') ?>" alt="<?= get_the_title() ?>">
                            <div class="bottom-corses-item">
                                <h5>Курс</h5>
                                <div>
                                    <h3><?= get_the_title() ?></h3>
                                    <p>Стоимость: <span><?= get_field('course_coast') ?></span></p>
                                    <p><?= get_field('course_duration') ?> <?= get_field('course_day') ?></p>
                                </div>
                            </div>
                            <a href="<?php the_permalink() ?>" class="link"><i>подробнее</i></a>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>

                </div>
            </div>
        </div>
    </section>

<?php get_template_part('template-parts/form_course'); ?>

    <section class="skills mobile-hidden">
        <div class="wrapper">
            <div class="row">
                <div class="skill-block">
                    <!--<img src="<?= get_template_directory_uri() ?>/img/1.svg" alt="skill A-level teens">-->
                    <div class="icon-svg">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="110"
                             height="98" viewBox="0 0 110 98">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #cfddf7;
                                    }
                                </style>
                            </defs>
                            <ellipse id="Эллипс_1" data-name="Эллипс 1" class="cls-1" cx="56" cy="88" rx="51" ry="7"/>
                            <image id="Слой_5" data-name="Слой 5" width="110" height="88"
                                   xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG4AAABYCAYAAAAUeMJLAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAXMklEQVR42u2de5BlV3Xef2vtfR739ms0TyReUoQESDIvAQIhMMSQQGwDBZHjSLLLKbCTIontEAwJJFVJeNpYqkocnAKMYjAiQZZc2AkJVVCAjQBJWBImyMjICL2RNNJo+nUf5+y9Vv44t2f0QKPp1sz0baSv6lTd2316n733Onuvtdf61mp4AlOH9/wPf+tvXdK87VD36GZ38gk8HN+9dd9P3fSjcPLVN/juR7rnCcFN8Le3L8bv/OD++c3uxye/7K+Q2mxgy2+//MrFNz3SfU8IDrjxlvvi4iDNW+jr92/eV25WP75/R1v+2f8bvqFekd88abtww73lsz/z9fbsH3fvwwT3vk/5BR+41M7brM4/XnHt3b79osvbt4+bwdvPf8UMb3ndPAuBX//c1+Mb/uKG/LSH3v8wwV1zx+qZ19y2dOZmD+RY4pSn70gL/bikeWCnnri9OdbP//Q3/JwP/eHqu25L4YOvOXEHP3d2zel7Ev/wpYLl1Xde/Kerb7nk2vSglScPbeTNFy1dODKrP/+Obf98Myfz8YDPfGl09he+O/779zZ55zYJb3v12TOc/5JAIUOEgiQD/vfVPS75suL18kUneLrjrBctXH3By8srHrbiQioSNrPZY3pc4Krr97/4Lp9/ktULbzt95wy/8lwoFQYSsRyJ1ud1Lyh4xknGuK3ffntz3FOvuK45B0De9ym/4Jo7Vs/UyiykIpWZZqWU2SKXqSn3lcW4aE5/crj+P14w+4ebPdCfRFx+VfuCP7lm+Ob9q3nbqcfHt/2Hc2t6hSICmPO7lzvX3QJFOfjAeS+rLvnZF9Z/DaBau2mZbGRWrxJnV0qZXUp5vrTcaItFrVLyMm72AH9S8eazimsvedv8e56zM/zVD+6c/S8fuSwTRQmuXPK1zFW3LrKrn37rnb/c++01of1YnPs79uHzP9S+f7MH9HjEb3zkzt9840WL/uWrzb//I/c3Xbjsb7pw+cKrbn/4QfxhOq4p95WDsNjf7EE8HvELr955acL+62e+tpdLr4Bx679/7gv6f3zWU+Seh977MMEV46KpmB1t9iAej3jZM8s7n/e0bX+11+f45k2LPHkm3fGPXxWu/HH3Pkx3nf7kcH1yfUKnbRJ++vT81Rtv6v1OE1L5sudWV3zsEe6TdbX6CPjq9X7il65efPVte/Wpq8QnttlDYDaOBk/bVd5y9ql847Vnzd7w4+75pfevvHdMVV76nuJdj9TOY1pZNw48/tFli7/8kcsHz26r/jusWkHyE+7PQ2G/beOeexq+e1e86N9/Yv/1733Ltosfes/TTixuact0SJ/pY1px7/7U+Ne+c2f+6DN3Gq8903jWCXOUfZAEEh1vVrBYM+POKmM0ziJtpvSIqLMiwmxjDKuEeolri1iBu6MYZhlVRURoLRNCQGyzp/5R4MpaJ90dkYNT7G4Mmoab7pzhc1ft5bbFXRc9a+fobz7wlt7H1vuYDS+P/3Xd4IybbmlOOmX3mAveOMMZJ0dmFcrxgNIzA2+JsWYuF+zTQNX2iSNBiayGAffHjISWIqyQBWLOhCSYGeIZEUFEcXfcfTLyI7KzH12ITfouqCrggCMCqkqvmuFZpxhvPXcXJ+3Kb7/lbp7++W8tnbbex2xYcN/8/vjsNsZ/8+ozezyl55ATURMh9MnibHNl5HBfBI2BpVqIZcbKVbzqEVLEmoJBmKWRxEoY4NFQDi4pEcHoLkEPCnCKsfaiddfBsZhZdzmEtMjTewNe+TxnXFXvvuKG6pz1PmfDOu7Wu/SpqPOcEwM+bInVHDlnht6iUWEMsYjMZRi1mVQJTWroDfpQrmI6S6lAk1mgTwiwai2VyAHRuQQEn7zBYJYwme5V54BbN4K1bR5xzHzyM6NI8+jKmNNPVMa6yM33zp603udsWHCDQd1XLyl8haqMjByaDLFswfoEAwtQpMRqGKJWMNP0GPWMSoT7JINnQh0ZAn1riQQsZ1DFUMyZrLbJ1iARl+ledSEIZoBMVh/gBiKREIScoKWlrvrU5khWRrmt1/ucDQvOg2rQySS2Be4DQl9pUp+5VVjaZoTxiP30mc9z7C2GUGfyOFCHGcpijHhFyIkqLbFcbWOG3KkxEZo2M2gS4zaR20TAQXzq9ZzZQR2HdFtmCIGqqqjrmhgc18RyapAwQ1/7NC3rNrk2LDgN2YbtIhJnkRyoqz7fvGmVP/uLFVZllbe+fA9nHF8zLJ2vtN/m9e0fUGjBH8+fxyuGz+c4h/G+byL3/wE7dJH+7ney0n8h6uAIg7ZhdZxx73ScoyiOHZmj51GDxmJijnQrztxoUyZLIkvLfJ2Z6/VYHo7QAO1ogIf5ddsaGzZOxBorwixZIq0sspLhc1/p86OVwHD/Hj565S24K6W0vD58ltzfS7Ixb2guYylkzMfkfZcS7XbuS7dR/ug/U2UFVdyEcdPStLkTWKhwDTiKSzHVV/ZAm4U2CyYKIYKWtFkYjBJBS4ZLY2Ko8AxRwYrBsROcqWvIgWoc8CoyEliwfd27pgDGwQ1AwX2yDbbgRXdJ2x36vAQrQPJGu7NlIG5QlJgZhAaTGk3p2G2VFk2tzeARHwv90njNy3eweOUiI72Ht7z0JEJwWko+b/+IfzD4ODFELivPZX4gqJSE7b9Iuv/j7NAxwz3/knEwNIMEpSoLkmXcDcuJgCDiiLebPfeHhKoSQvfZ3TsBkSmKgqo6ci7gDbeUPcWoBgox15AGPOeZs5x2ygILK7C8YDTjATqe5afludxUfZie9ciLkAOsFIli+zkUCy/mvjRkUC0wQwsCgtMvCnDpjBNzAgY4OuU6zlL7IONEZWKclJG6KoB0RJ6z8VfABNdMBmJUJBvKCmURGPYr6mWlrWeZzy37w4heLpgfw6gyAmMaKYnekKMyjgvMWYsREM/gTlkEYgwYVed1ANzpLMspRtAHHwfgoOsrBI6U3DYuuGhlCiK0MiRKD8IM2YzcGCEYIWRECtpQUCRlVMGSDpkZ1gyrRN97lB4gZWYFQihYpaXSzneiGIgi7pOTHLinA5+nFTkddNEdOICrY5bJCcIR6v+GBScJc7zTY5YRD4QQ6ANtToRCaCyxqBHqQDaYKUuG9Zgm1vjQSSbURWA5rhCy0dNZ8kRPiwjiGeuW2YG3dtpjDw6I6sFv7p29trZRHKH37jHNg+RIkBLB8YkJ6XmAAvvFqHGOayG3mW0jJzcBHdfIeESODVoZ/TygRJm1GkmCPaBLXZSguxx7kKd9WrGm39ac5AcmWnXidD4y2PgBvFBTL7HWQJygDWCsxh4hCTNiZBswCs7ODMthgIU+RRuYyX3EnBVR2twn+ICknTtLXXEX3A13O7jdrOm36VZx4HrwJX5AWMe9C+scqZdvw4IbZ+pUtvyLjycQAS8wN1IcPOAuAdbMdwUeSmUZP+Dz8DCeOt1HAYAyhW46yLh3MURQcnJUA9kzKpMzrDttjOQc1i2HDQsulHVqU6DUFcYpoGUki1K2074kji6SanfelIg5tGbgnd5TVVJwgjvWNFQx0/oOijKu29bc+FY5GtlxNuBj/3YXPW/AA2bl4z5xSzonUcdEpnrQ78xArEA1g/QZSsmvfXAvg9Q/dp6TXBBHTU0A8JKUgQKC/+S7rQ4FcRAMywejBActYiHHiLWBGAJBYCQ1uVi/HDbu8qJVqfpoakBKYoTMmIe+ZY83dFGn0LlnAQSyOSqCKAhjQqzAQFODVBXm63cyb3yrlNKGbUOMFdkGqEQMCFuAXnA0YdnQEPDsaOis4TixJC1lLIJKg0kihoJhOyYU5bHbKkM7TkVZwLAgVAVkKBxSfHxvlWFiUUpQEEjJcM8URQB1ilSBQJAShlCXY6wZHzvjpNL5Zjnth2qWVVmh1j4hK9HCZs/dpiJpR1VQhZRg794l3DN79uwghG7/zMEY+YCZapZRGjOn29adBbthwY18WAftA05PZrqzsR6BbdIEl45Ukya8SjMjakHOTgiTbce6w3lKRozrYIAd5fbFMy6RMBHe3YOIBuX4AOaGToRX0xG+g/Rp8nDdBQOmzniXAEzczFgXxokaOu6GCngG7MBExqgHJvpwvBJHu/1AJtAdCVILM8Hp0ZJHGRElZ0dFUVEwQwOo6rHTcUcPD+RVhgPM4JyayZs/8QNiEzfYAQ7YVLRvHrj53gFLSwMKKRl6pNSKH961nyEFu8sRu3btoigEzCZMsPXbBVO34gw/MLlBQudgz0xcR+BEQNHQkXK6CLOh2vEuN7v9xiMunX9Vi0gm0pgCJZYVmfyum/2NT/8UrrjOk+7ZEFUse+dVVyVZJgjk3HkoRAJOIudJXoEIh6OKjmb7dTBO3FETthWgge/dMcDaxMnHzxMLSDZLfMCsPzS/4HAxlYLrtg9DUFQFVxgluH9xxPJyxvKYhbmaHTvmKEPEpZtcAeQwUvuOZvtma7trBoOmHeLuxNi53GNcixQw0YW+IWr91AlOPGCWCTrJehElAXv3rbK42hDCHCkLi4OE6wq7d8x2yp5uBT3aHBzt9hElCUQtcYHZ+RnEDdeOtaATwXa7ZOd49taOHT3vqAluUiYCcSy32CS6tTxMZApGLZgUNMlZHY4wp7PQXDkcHX+029e8TJzwzoMaT95ec+LOEmWMkhDtnp2t49YIAZH1J49N3Yo7PEx0wlGjo2+8fQ+zCF14wB0qdSDghG6VTFasqnbUUzPA1y2HqVtx7oAKuKChQOl4JnO9SKClLkC9oYzCTL9CpTvYTmZj09s/Vpi6FeeSJ6a3oVp0TlqFXdtniAGWlwcIYxb6fXZsmyFK52IyHMkTH+Emtn+sMHWCAybmcXeusol3oxehOK7m+O2hs9y001fm1qVrTSgCh2OhHe32jwWm4/V5CMyse7Odzlx3wy0RxTozOnS7He6IGyGELqvHDk/HH+32jwWmTnB6MI2R7LmjpAfIuTPpxDtXlKWMOKh2no41p/Bmt3+sMIVbpcIBelteI28QYgmTxA+YGHy6EavyaLd/7GZpqtBRViaTq4LhJOuqMOTuUNWtks7Nz1qGkmo4LHfX0W7/WGHqBIc6Io67ECQirgSJHau5i8R0HgiB7AmJnW/RPSHyUCbxwy8msjFvCTo5AFOQJ/EzvO3azyvQDokoCUcS5CkqsjKFW+WhodKiXuCpIWgJNoJYk00eVPHgkf++I7SD4BNPk/garS6DBhJGjLMkAeu+EQOYa5eLPgXYcoIb40QSqTAqmwxh4r0fe0vxKE7gLj/bECKIdjSDteCqGN5GZC2NTSGGTnAkKCfhu2nAlHTj8FHlkiAjGmrGCpBAG2ihEkGNQ16CodkRUyR3PyM5tC3igsS1FLaWaI5S0ibHy5ZxGDyWrh9RbLkVhzgNs/zqB+5iVO6mT2A17COI0ZoT/NBkJdOEuCLeoi4HeP7JjaIoCKMxQQdc/I7dFAKajF5QvAlUsZiaOitbT3CNoLGh3r7A/atCkQyYo5AIbsijuPCDTwpvZEeYuL9wREpSo2g5oggzlFiXvqyRbE4ujUyi9mKzZwDYgoKzusUpKYshO2yZ9/2zeZ60sErfhli7gD7KvLpMMlwzE69XBgldTO5e59cvhh07+oRE56QMRjAFyZRaTIlpsgV1nOCEDMdtM2Izy/DeFcRnQObQ2AnmUJfhXX620JGEvCXhtA57F1e6MlbzggUhlSXjsAQRTANMUS3O6enJYUJc8Qwn7RmQqvv5/r2zXSTbW1JoSWJkEbII5oKbdEaHg1gmJMXYTxMAy6iX4EJh8Nd3zjEutvG83jI6oQ0VNovZiOgRD9NzjttygkvZCGXitN27GObMDbcbpSmkCs0FRQrEDNE6Orio45JxMQhd8BLfhuYGvCBnJcoqSeGaW++nHCee+YwZ0ETOjkhAtZ5skdNDr99ygiMK5MipOxvm6m1cd/MK9y1N3FJ2sKpR9pbkLVkMkwAScFeaCpIMKUQgJygdT8odi87Nd7Tsiqv8nRMVaLooNmvMZkemyOd1QHCfvsLPOe/DS+//7Ff9xZvdqUOjAIEnbRee89RE0povfS/RlmMQY5CNZYPlFFgaB+5fgfsWW/btH7O42LBvtMriIJBbJ8eIeovFHp//+oAo23nJaQXzk210UskKWzukT1HlvgOC+8pVN79yfyze/bVv3f2Kze7UoRA6bxXuys+fXVCnIZ+9eh/fvqtiaeyMR9AOlGagpJHiKRCkwLWiEaEeVVSp5J7lgnuHsLoSuPoG+PPvQaFjfualk1qbRDLWhXZC6lbeNAru7/7UCV8+2ap//ZKzZq98LA0ebYiPyRmyBJ6xs+A1zy/wvJ1P/knixn2RlJZQVil1QPQRwVtcMqZ5kkmj0C5SVQ2NwnfuDvy3L9xPG+DnzpnhhPkEmU6v5bXiHY45INNzejrQk/P/XvUN4Bu/t9k9elSUaGxpvaAM8MZz+tx255Dv3r6PT3y+5hde2Wf39pJd81B4JjdDPINLjWjBuIToCyyGzFXXjfnSlftYRfiZ0+b52Rd3liciZEKXFpXoAqu0MOGobAYu/MzoTYu+tO0/nb/7YtiCB3CygAoqLUMvOK40/tW5PX77/xR8+wfO738ucfIJxmlPF07ZkzjhuILZXoFkZbzq3HVf5nt7E1f8MLF4T5+C43nNC+CCl8OMDMF6EJdorabQErKDFoi2HR1zk4Z97c1y5lI9e+Cf7245weXYEIhEd6IkkiX6vZp3vzHyl9f9iE/+5QI/uHeR790NWtRdwZg0RgUiThsGVL4TWWk5ec8Sr31VzZmnBno0kKsux8/nqCacEy8BnMIjm7Hc3vM/x2+9/taV05Nok3WoP3/R4Peev2flui0nOJECEHIyQgzE0NUOiyHywucfzxnPgxtvrrj+b1r+9k7lrkVl1RwqJ1bCCTU8+2lDXvTMHqfu2cZMBFrIUtCGTrjTBM3R6liMVvJwdq6dWYmtpQWZ37/lBGeda/EBxB1FaCBDFSJ9hxedCC88qeqS5wFcJrrLSbqTJo0QHVBqn5S6bScIhLyMh9nNHuKD8N7zw8UAv3jR0gdHaan+7Lt3vOsStuBWuZbqKwEsdQXaYiwBI+URWWosCEgikXG6DNAQCkBRG9CPBZ2HuZ3MwBrza46pIpY8AKfsmbnRQtLPTb5vOcFBIuVE1AINAcuKdpQs4iSSHegyS6HojIm1suTmDEOfEgjmkDJohABjWkzG1Kz7XwAcE6ytvDVsOcGZZWJQhI68qjHguSO5hihg2tX7N8MmZSt8jWYXAr2uehUWElKC44gJFV2B72kJlD4atpzglIoJqY5kLTEECHRnLoxMJmhAJaBrNHPveCaIQWm4ARa6gqC5q/WMbC237ZYTnHjn/TCcOKkWmLOjoaPoaegqT2RrUfGuah0d+0sIZNaSbqw7ok2SOAzHvUWZjgj3o2Hjtbw8qrYZTFAdM0oVRQFHPWTV8VUJVAcKKESRyefywFEryEQAduDPYJK+e+CeB3zuaj2vu9zIupEUWoeaAXgfbZQYeseushDlmL7AII7p54AWCcsJD1vjjd0sRJsUY2v7jAoIxT5GRTh2/xRpblws3SuJ1UFA+5EaSCESbWso981CmxsKKlKRWRm1DGQ328e6f73tbFgjF73+paXM8+Vrlylz2x2JcsZFnrgOcRWxwtuW4IEvXu1ITsQ4OHY54Ht2DvfuvWvxn37xyh0ffeEZgZMXgDz9NZM3HSZYLPjhSssXv2WEovnVk548/6frbeYxObvPuyj9yjgt//dYO687YxuvOkt4ynSeX6cGdw3hz68Z83+/k1ldhboXfukzv1F/er3tPCbBfeHarJ/40up5mapO2ny87yskX9jsuZlq9G2F/dajKop/ErRtzjunvuz1ZxXrLnt4RMJL/+6Pll586z1zz1q2YbT1F4J7XCHZKO6sFlaO375y84cuOO4bG23n/wPJ/HcgR5a2QwAAAABJRU5ErkJggg=="/>
                        </svg>
                    </div>
                    <p>Устанавливать компьютерные программы</p>
                </div>
                <div class="skill-block">
                    <!--<img src="<?= get_template_directory_uri() ?>/img/2.svg"  alt="skill A-level teens">-->
                    <div class="icon-svg">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="101"
                             height="108" viewBox="0 0 101 108">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #d1dfec;
                                    }
                                </style>
                            </defs>
                            <ellipse id="Эллипс_1" data-name="Эллипс 1" class="cls-1" cx="51" cy="99" rx="51" ry="7"/>
                            <image id="Слой_4" data-name="Слой 4" width="98" height="103"
                                   xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABnCAYAAAD7aJe9AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAeZElEQVR42uWde5Dlx1XfP+d09+/3u3NndyWt5Je8km1hSwbH2EY2Ii4T4xibR+GQCoRHYQooyoQqE14hUCThURQVV0EopyAhKYpgIMEEXMHFMwgCmKQwNgb5VUI2tmRLyA9JlvYxcx+/7j4nf/Sd1Wu1Mzt3RrMUZ+vWzNy9t3/96++vu8/5nkcLl5nc/sC4cdvtvOgDd4wv/OhZv+Fin3ULR9pXl6gboc6eMiw//bIbN9/9Nf84vnu/bcmR3slj5K1/sLjl1vecfs1D4YofnafEUJe79N6OtL+9OaMrJfaI158Z7IHFq1985R9826s3br3Uti4bIH74N7e+6c8+HF/+DNn+1i/6rKt4wU0jp64GEUFVEWlddffWcZHzvx+VGGdZLq/koTPO//2bLd75/sR2N3vzjddPP/Smrxr+y6W0dVkA8fP/5/SrfvsvNr/iuuPyXV/3xcqpp0GUjIxtwHdAuNykhiVeNvACeQr33gdv/b1Pce9DV//Ul7+s/s63vHr4o722pY994/t/xf7Fv3mrf+uTdTO3n64bt77XXzt05bu+5pXKC55qaH0ANXvUTNgRdz/ymbAjYhHzkb43NvKCm07CP3/V0+hj/p7fuY0v//D93u21rccBcdcnZ8++8xPnbthrA+vKO287fUsdp//6FS8o3PgcZzZmglxN8XBBEC4nSaYMyShuWB6wOTz/Onj5zVCQ73n3Hflle23rcUDM6nJjdNkzkuvK7fekz+6D8tJnT1luL6FXkoBpftTnHgnC5bJULV2oiwGrQhgcUmE8d5rPfVZgYMFffWT2kr229TggJuHYwmt90m7mrtPx2duWefqV0GuiF8HOVZL05wf/cgQBoPQCCl0UrJ4BK1wxuYJnHEvgkU+eDk/fa1vxh35t+1vuuMtvHCndZgxbD1ne6AOLr/jJcz+dSKNT9EXX+20//NXHfukwbmY5Z5hEpZ/MkSWU2jH0gdkI6KMfiEeC4O5HD4pluhhYWEbjcWoRbAlDyGQNWO2HvTalo53rlpFB0kCed10UKVG0JBtGzPEiSMl7be+S5XjibFoUaq64DMykci6MiGyfH+jHak6Xy14x9UBZzvA0sFUVGYScM5PYMfpI9RD32lZ809c+7VH67jf8+PLHtiObv/H96bt33nv7Id7MOC66lCJBO8LWaa46diXzumTZ9fRWLvidvU6EUDosGaNkihc6SQQLYEIUGPXiBqHvZi/WjPbHkTxnyoR+sYVME1sVVHqib497BuKxbyzS9mAp6F4bWFc8BWrdYKEBO9Hz0GLOhz4x58xWYrnm0lN1gbqioRBYQu2QOoVQqPIQzubF+2YXv74EJS/PMPQjQ+q45YZNohegkmSB+94pmMcBEXVaypI9T6l1RePUajnL8UWkMjCdTHhgtsVX33IVifWWILEFEPCy2SzXCFWWuEWiXgtrMiRlNUqjwx+969N05SrcnUnX4XUA2fuS/rgBv+mp9iFz0bcf7Hg/oWyX2eYkTKhJWMgCGcFkg87BdL2RWoYOJaCrB9NxoEMUzEDientfHA20JwaYjxkJitkSd6jMCZfwPD/ukz/y+slbDny0LyLTHLZ6UXTZEYeCdkopI5QpktZjVxNGAHDAClUrohHFQTOF9cyl2AnLAhVjETebpmQTYq4gx4D53ts6iMH8nQ/4Z7/j/Q++8v7P6NWL2XSyKHUIEy/kiy8tgdFmOgw5jnzzz5/F+uOcmH2C0xsn+e13LlBbUzsKUBfOEHqGSeGpJxe84sUneMWNwjGvRF1vK/Ra6QMgoBKxPJKi0LHEcAJ7b39tIH74v3/qm3727Z/6vHDFsTeWsZBcmW4kZuODEPuLfrerx+l0BjqDeJJcRhb9JiFlFjMn7V37u6CkceRYf4KtZWE2bnHmwSl//baz/OWznW/8Z1dw3cW7t6t2llNsQ22ZpSoaOqoZEBCdgG0/OUB87y8u33jn3f7U51/TvfGVtwife11mimLAGHuCXdyeCaYUGSgYvQbEK+7HcFkQmVApe+vIE8gyGXEsKLDtm3zgb3v++M8Df/2JJT/16wve/I0XR8J32c2TVcQTaCF5pYuwGDPuRsEvaXD3PTff9j6/+cP3+Y3PunLr337nP3Veet1IkISrELSipujqAk/0cq0EEXqZglecAGIIHZVKY+n3/4pZ6UUIEhl04POfmfnu1wWee0Xmzvvg198viM+w2gxGkxnmSxBBiuBiuBjVrU0PEQw//1NIuIKtHr+cMxMC22HKRl1c0njuG4j33X76hV2wN9780pMMvSOiqED1Ah7WafoARcEDxTIqIKIMvXPzS6+iC8b7P7jVbAtt+7kyJehAbX8gNaGeiJLwonhRpEbEleAHq+Hve7Q++rfLG6SO3HQjqIzgStMSnGKAPGmmyBOLRIz2MLvPEQIqqz5b5u775nhUTMGqU3IGDJPSMPTGdVmtQEUD51Vh1rRxHiv7BmLB8UFNmcg2ZoYA6opEwakoR+vYBxAPzXYIrW+6cq8ObKOmLPImBcFZElSIHlh9HKhIDJg4HgSJARcwcSplbRvnsbJvIEyiqvSEoogOIEKib2unjrvzNE+CuIHoiOEk+vYMS49mQaUH7xEUbws94gqmmMe2vBp4EQKKONRiWHGUeOAP2r6BcIrW6vQ6wQgUL4hDriMhCHIZEKTijmrrkzgULxiBIWxQq4OcRQSCgMsIXQVxnCVoRqgErUABClEKMVZEMthy3e49SvYNRBAvVQrqULBmPBUggNUIsp7qeRAiWltfAlgFV6esrO1CJnSVZTZGEkUiRQIZoUhPdcEk4BowItmgaqS6kKvjmg60r/veUaUY2kMuS7yLpJCwOaQ+4fOIdmNTR49QVAwbO9KJ1rcQI7VWSs2E3il5yl0fm1M1EmXEveKlR/qElhmjK6oKYrg7IQg5L+m6jlOnOo4d4Ia9byBchWVNmHYc8yU+n7MYNvFZRXswq9QjVmEDrS91btQehsU2Q0ps0TOakyTg0uwGqxFQNCqjFZIHAgGTgmqHLAuVjOsUGyvHlLXZ2wMBAiBJo5nnHiip588/WlnkLRbLkdQHvB6eZ28vspu/IEvCYsEERBUTo9bCwisaFdwZWaBeSb2CGm4F00IhEw/QW7BWS14HJjGQx5FOMmnxIK/9/KfSrTiaeMTbxG7+AhHAA9Wkee1iwkVIYm0ua0K9EkJCilC8UE2IEtcdusfJWq1V5riDa0G1Zz5mNqApGbbEu6Ndmnb1F/iERMRdkGq4F4ImggrkEVxIvTa6I5emXamS1AkuHKRRtx6scgzPRtEJpsIibjLDCCh9F6hHHNG5m7+g2oIqBYmREIRSMuaGhAhmGBnUKN5ACEPADKpXjEI4QGVkLSCMbToVaujJ2VGJbAiNHsgQnzzX9wVlN3+BxGNkc4oJXYhUDVgx3ANRBDzg1pYjV8UMFlnpNeES4QCjSdYCQrR5oaoFNChLLRTLoD2mke6Iretd/QXjko0gLIFQCyk1CmT0QrIM3mFaiBJJ6lSv9JrYCAoZDlI7XwuIguPepvCQepIXojjUDpeMy9HaEbv5C65Mp7nh2pNIjFBAI+BQxFeBC0ohA5HgglHaTMgQ42fAr7o8gNioC7bDlIkVxpxRDOM4oo54auylRsZqpFBRT+ArF7IWZM1HyqmoxbYTCZhkcg10QcHKeX+BM0U5/Qh/Qc9GnTOXTWKM4BU0YA7qFcVXxqg/IgDAG7/kDhGcqw50BzxUrlpWo94HbTdbCxAb/2917c2uUBHiyrAqaNi5Vhu4o5Zs/QWjE37wf87fkMZh/NHXy1t23jtcp4GWxstI87yptA1dmpLeOKr1LoCzmmFeMASsIl5JoYIdrU9kGuPWhd7/wN3pH6gsDHjLznuH2tPtMuHeT2yzKIUQHa8FSsUEahhRX0+rMjFC7VAHYkFCpJbKEJ1rnzFlqkc7K2pZXHB8j8vybH6M5+xQgYgRlnVkWQNBHBVD1HAVHMXXDKl0FAuOm+NiWC3UKohUYpweKBe0L9GH7+9Nv+pfe9tdD75Yu84eIhzvsoyv+8mH/mMyLZ93nf3loQKRDGJJDDLBauP3o0SoK//FmkCoe2tPKqUoQiBJIpY56ahBAKI9HIZyv41XnwuTTbWJhW5u6AytJy3rGBeyNRwqEC4j6DaqEfMlIiuHhQcEP+8T3q+IBiCDVISKe2wPoW3j0oEfrM/gUsWsnl97/8PX9z+z8/vX/fjf/vvSXRl//fvk+wB+k0Nemlw6sgWqBFx70EDOhpsSUlwfCAmMuSDqhBSpFjELBAsrII52j5j1deNC78/7pw2x+qMo0UNXK4JAEG8erlpAKn0UfDQsrAeEViEmY/RMzQGVSBLfc9LyRLYopW8GXTN5cFrAQNiLQcfFDTq1YxdcIG85dfpd5/JVj8oJOA/EHR/7zABw07NOXlpk1EWk0KzUWguoICngZlRAY8BlzVw9CVQpLSBMA1acZS0EcQq7P2Xn8hV89N6RZR0JBpIEL5UxQCoriiNm8EgSpVLJ1iiOG649SQwXn3HqiwsC8QNfc/WvPva9CHDHx88OizENq9+56frjBwNGgYkOVI9UB3Ew81a5oVY0rLdZS62EAKoRlYh7ICgEGdpTsJt23PXM6vbDpF+tWBU8hqbxeMDFqSYUEzRGliaAQWJXrUx9vmeVIULLSStu538/KBnikmdfmwihozpoMFRaCIvVPQzUbmIt4MsxzMGqEgRqNVJcgl087F7KjKSZECLBR6gjQRXECT62MAM1xCFkCBogJaIXxCO7sX6JE5eWuvX8Z51YfPieswbwvFN7//KuUgMphkZDQxt58/N/rxv7JAAO4kIQIazGRaWHWnfdK4JEghu1CrUYqpGgibE6VEU94aGSPOJeqIuCaSKEgLJiCC8i2Rd7TsA4v4w+79TxgwNgJRYMaCUbtPEaVJr9IGsuSysM8NW/sDJUzQ3R5j3TXcoIVYFMoYgSghJjoDpUnJgSSCR7JsRG/hUpVDOyO1WcuMviIZ1d2tJ0WOJ0zZVa4TwjbivSz/eeHfqE7a/acFuF3tLilyTstH3xkXKtIJWgQlCnSgaD7JWkbfYauWlUqgQFFwdvAWe7DV9dbO49vfcwgZBCWy6ktFFzQTUChnleTe/9i3lBJbXYIysgjooAEdvDZp2sQ8sImpAqBNry5OKE0tIEujCg0sGy4NEQImpCJLEb0IMu96z0HC4Q2vTuESVpRT2u/BGKh4CtuVs7ATdtW4HGlT9C6aRde7fN9KwFNEVqXkDsKQixjqhI2/yloK64jXh0QghY3ka7jnMGx3bp31LLpe8RhwLEZe6POJ7O8dxnTgihwz1QEJI7pkIwwWUnpB+qlTbzGDAzohrYwbmG/n77I/KSpBPAoBohNuNAW4zQw1qZtGBmrEAQQhCwDGtmpT5S/n77I8Im7hEzWQUBQUAp5sQ2nzHjvFpc3KBACNoepnWzXp8sIC53f4SRMYbzFn4FsIwHPR9ALerUlfodYvNmmxcEQQ8wtvfvtT9Cq7BjzgQAg+AQkDZzAQ0Bq9YCFUJAaFrZQc4GOHQa/PL2R4istDjTZuUHQNIj/r/9bGA0G6X9Dc1s/zuyNF3u/ogiI64DTsZCBIRqyxY4E7Sl97LaJ2JbqswNlxY4FC6h+syRAgGXtz9iqM2SFk3E6i1OVDoqgnjzR4is7J/seCoEElQIsg27lBm6FDlUIC53fwRDYmEgCn0QGrfd+u0qtNUqogEsCE6gSKPz+6CXT6LKrnKZ+yO2Fj133bvNaJVY68oFoCxVGFwxFawuCd5TpbayQtYzxMT1z5wyHGDG5qECcbn7I2Iv5LrERKjach+CJ3xlTXsQzANBA+6OCbhUxlrRMPwdmhGXuT8Cc9wMTT3qO+OqiNXmrbPwiL0iIBpbvG7OB54dGAHe9i6/+Xff+ckv+9IXPv13v/pV8p6Davxy90cEiwTvMOvw4qCN2oghkaRgKmAKNeLSKipE6wh+8HUVIsBv/elDX3FPPPlDt77HCnBgQFzu/gjxBaJjAyA6qFBLXamnGUMQaTyWqIKOQAUz3BPCwcVNRYAv+vwr//j33zfvXvMSvfXnDqzpy98f4TKAG7VGxNvxCNUyGrXNYBEcp3ok0GHmYIkoRiP8Dniz/qYvlD8B/uStBwgCgISKEVYJ5QVxwSsQFAvSal+sA4Su2qggodHWxWMzkEN9eJo8gSxjAMkEWWI6UKUjRKHWTJWWR1dckKS41NZ/mVGkkGWD/gBZjkP2R8jDSwfNu7XaKgisX68jiKycQu1q7hV1GmXtu697Pq8MGineEm1KMEKpSBCiVQgVMagrbkPF6VRJqyCzyyZ1a9cbrUskTFYD07QnKhQHTay9STjN1xRXPJx4aED76tp68VJ2xyYLnnXtlJDAJFBpJl2lkX8ujtAMO2dVF82hliUp7hQIOxg53BkRK7kuIPTNMQQQrC0hgK5pWRsgqbSKY/jKYaZgS1Ksu+r5ZXTiSucN1maSVGv+htKodQkB8TbpzpOA2kO1Ay3SdqhALPMmH79nztIKTiFIRaypgsXzKoN//1K8ECUh7rgWqgcE6NW5/tQm/S4hkUhoqchmTUNyedj0iNJy5lYkq7mz879thq+m3gHJoQIREow+JzNAqJhUVA0h0AJV1q0RYS0wmIqJYe5QHfEFIU12nRFVR5Ae10xYLU1oazF4wMWawaiyKtco59lXF0cPMDXsUIEwWnmdGFMj0rxQa21Vw1QOwI4QqjlOXfkSQqunUeZYW7gu/n02MFOgfxRjvsrZPx+DIDRDXVuiadsv9mCnXIocKhDahhwzx6ySUtPPa6FFcKxdby5gGDFGTIycHy45rXvgwoUZMEW1AqGRkULjwlybxoetKo1mkIBKYwraQnVww3c51AQ9OvEOp1UlA9oxAzW2hyU0K1pWAy/aiqe06I6AuOJkxEDZxlBSSsypTOs2szDgsvfxPeSlqVkProJqwKxg1RASlbq2z9q9IgillFbJMiYcWUVl7CicTyyCEQSyGYgRQoQFLGum7wXZRT0ddVX92yJZhLGASkIw4iWmwx/qjFDaOXKlZPA28CGEVnzxALh8kVbeLYTVUWleKSW30xz3cGtSm7KQQmz0Rs2QYOjDI6yHJ36lXIgGURO9GVbHVTHGitu5S7qXQ50RNUMnE9wjXhtVvaO+2k4ExhpSvLSANQ+PUF8jnQg1Q9zF3gr9wJjBY2wBxqHZEwWlmLRovosNntLKEK2IQU2b5OIE23kQnuRjC55I+rTFs6+PsKp6r0RwaQ4WdO24IGtESQswk7DSViPUSgpbYNOLf3+5TRd7XCHnSoirPcCg18gq0umJZbTV55ShbJHsGJUlkiL4NqJ7TzI+XIqjBFIYVmrg6rjLqrhDSHnXNXg3UTEsxxabFFehkiKIDniZn+e1nkj6WLCxoH0khnA+Pz3AqtDuxWnunRDj0WHSJbwa4hGJ3YqG2ruddLgUR+hb1RgDkdrSnbRxQw4r79c6F2jRhDvLtlNxa35rCf2uav5HwlP4jdvPcGa2IPUdriCzDF3XTgfe02FPtRVvHK5mjIHohXmpSFhc0sEFh5yo4ri3oOm48gbtBHJVnHWddHUnMNJAQqvYX2nEnPAwJfFEcqo+wCuvn+LpBMUqGoUuCyVI8//sUkQ4kFHdJK8KwA95C1mVM80+EC+hCPGaNf0KvY1s09GbkUPX6nGHykIiQw3nrdSdS8kqKTMeQCT1+TYEsHathxeTwDzAQMFrJIcOtcC2KFMbcTInvPCATni6go4BU8jdDOoGNWSMxEQKWpXsYEWZTkfmS8PigJmQy8hG7FkyMtN2jT446otLOjFgzSqXkexCiNC7onmLIBtgCwYdKOGgPbuXJhMq1DmBTULZZogbjAZjBdeOe/JxnrkB47ltVCYMBlsuxHGGDi1qPc8yfadoL1R6FqMjPiA+AgFVxawxsSGE5ntxRyRgjT85fCBEpEU7hIRJqzzc9oR2aKpy4PmRlyTFElEHQKCOZLPme65gZhxLD7Lc3oS4QQ6VsSzxNCUUqHlB1cR0U8mLZYtUDIZ6R6+Qzal4A6K08yZEVqTg6vpN/3oygLCRod/AAuStbaaaqRVCTNTqhCfvNOYL908BCYxL50TneF6ShgkaIKmxFU/whrdsU3NH3SgsaiH5Fhtlk8mQ+KLnLnjFSxMnugEVxVggHlbFJHtspd62GaDYzlEsLliFcAlnTKwFRB+VPFZyFyn9lHys4223nWYGLQGwPHnHMV9ITAUNxlSEOrmGs6FnEEjWEhdDXpJ8RMcFkxQZRPFyBvXAbFb4/Q86RQNf/g8TXYXoCZUldWWzNHZgdaCtBKq1qsnNiSSXpBWuBcRidDQGeoWpZV5+KsG4SZhEwjIj4YhnhAvLoNR5Jm50bFhh9HYySq7tAPOffcPViDq5OJorZWMD8Z477+v5kd8y3vM+4bUvLyQbiWzg6gRVimQCcv44550hl5Y9hmoA2/vSvJ7WFDsKipcRKZlpDx4Fq5lkCxZhY63m15U+b6NxivQB95E8jtSuRzRiWul8oI7gaYQY6egxj9SseAGpI0NX6XRC0IR5pZYW1hm6AvVhHc297RfusJgvEFGmk42tvfZ1vSLt7ri0IGA3IReoElk6HO9agfOjFOuPc6ZAD8RqaBxaVfxVts+MGW/8H9tk6xlZovU40eckDSw9s8mEz7lpjo8Jt46aZqhuYNmRYqus0zYrdvaJUozt7W3Mpkw3hz2fCLhvIIJlKwFibZ4yWxUT6WtjWTO+K5NUPBDlNMIV1DiHMRI1cOdDyq23bfG+ezYYbMbzr9vgtS9RTj3FkbGSiS2geZf0qeyVCULnTg5CdUGqkJyWZdQL40wJ8TjTsmDpW/hmpM4GhknidTd8kpfdfIrUQ14uiDZQqyPRMA0Ei3hdYJ1TaktyzJ3xJ3eCZuWLT933hz972ECI7F01e0Iw1RC5goXNiZYIKPecUX7hN7f5RJ5DCFgZec9fVz54n/B9XzLl2hOgaYnkHnS9GXf8TOAnvuskIufwOpBk4BwPMRFDfcDqKWKEMttiCEpEyO4ISrXGvGo3EAwmXvAKH7s/8q73FuoQ3/y5L7z6vXvty/5P3XL5tRUg+x4Io+0nQSbNpRoKv/fOwr2zns++7gre9FUDP/ENV/Giz1LObm/w9tsCHYKVnukBnE2x1a0qIiwjitKZcUImxCJ0o0EHC8sQE542mJuwcGWJsPDERp+ZyIIwniGI8jf3Kb9862mWuvF1t9ykP3bDU8Lh2xGmKaybIOBSiVTEEirKIjgf/uSSGI3X/6MJ1wwjo/V8/Rf2/MUvGX9z94xZ7FEf2R4c2WcUxU5Nqkn8NIvxKib9hGwwLxlPHUG15XPMl2ykFqNsy0wi0SVa0g2FscB8NnD/2YG/+qjzp3dUztTh2z/r6fff9wNfdvzBS+nTvoHI6EvikRdWXU8+wzP5V7/wEItFpY8BJDAP59DtgW4imBjuFTNbJb4XUGesC1KnbGQoBgsNWNTvjPnM2Vc9f/h/3/NPrvvIpfZl30CotpoXvkZ8vXhoUUkKQYxozvOe3vPnd8Evv8P5xi/o6CP8yp8u8TjhuddtsFEqM+k4nmHWXXyPaBrNo99z9/OcbO/zb8ulLtLkWGfLxWLpdWBQ4tDHzPaCEqOLaowtsLnWbBgWQsJNOIPGY10++4zNcvoZ19RP/LuvPPn+39jneO4fiJpHX/md21T3VdiJrF7sml6rOBoSo80RSYQa+NIvUO759Da33z3jB+7dJJY5vgwcPznjK188ZcTRuGTb+z2552UnUHmVMPPIgIJrRh74bz94zf/a7xgcpOx7s+70Es8JvoBUU9xOMzBBNGNUTp0wvvl1U176nAmd9mzEjpuff4zv/ZLjXPuUFmtkpcfjmvuTO2emHD+EMd2X7HtGPOuEfOyuM4F3f9R55Y3CrBRqGBiLM0UYfUmOF3c1qlSMY7QQ8Q60HQ5y3ZWVb33VhOZi2/lZW0EYEZQWYLwbFF1ZUmXCzIQUlFAzYWK84yOFEJbcOEl3HDUA58div1983nXpznlN3/GO27f48LZSugR5hnaVrbAkJ20n5h7hy0KHyZKNPhNtGwb4+LnIn723UBZXfscN1/cfO2oAdmQtZ+Ubf277NZ/6DE879VT9xde8KPKC6yNFF3TDgMwg7BaNfchSDeKkkucVrz0fvBv+4C8zd983vv4p1+gD/+kNG//7SDv4CFk7tfNf/uf7X/3JrZPPIc/+ax8LtY9sL5yJbJJ1z1TLoUgl4TZj2kXK0qi+QdD47U87/sBHfvrbr/nDI+3cY+RAaqH9zK2L53zo4/k55+Z6/PRWvYKgDH1eLHM/rN/6/sWTIcuoUnMZTtji+AZnP+faePt3vGb6saPs14Xk/wM06n19AyQJQQAAAABJRU5ErkJggg=="/>
                        </svg>
                    </div>
                    <p>Создавать игры</p>
                </div>
                <div class="skill-block">
                    <!--<img src="<?= get_template_directory_uri() ?>/img/3.svg" alt="skill A-level teens">-->
                    <div class="icon-svg">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100"
                             height="100" viewBox="0 0 100 100">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #d1dfec;
                                    }
                                </style>
                            </defs>
                            <ellipse id="Эллипс_1" data-name="Эллипс 1" class="cls-1" cx="48" cy="91" rx="51" ry="7"/>
                            <image id="Слой_2" data-name="Слой 2" width="100" height="91"
                                   xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABbCAYAAACFziAsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAcKklEQVR42u2de7RlV1Xmf3Outfbe55x7bz3ygiSoUEkwhIAmhAYhIdBRQcAmiCJIoE07hnYPGuh0WvAB3TqGLWnSiqKjG9sHAwwvDdJCQwAhaHgFFAJEXvJICGgSqFTVvfecs/dej9l/7FuV0lTqVnJvpW7Eb4xT59YZe68z1/rWmmvOueeaBw6Bl1+1+9L/dtVt/5Z/wX0Of6gPP3nrjnMb69pjLdx3IvRQH4qTso+4dKyF+07EoQmZ7WWn2B3HWrjvRAjAK96y+9Ibbll4pFIVm/catse+dJUuzGar+xa2bS+KnnVC97ev/Knmfx9rgf+5QwEa0zaUNhX2ajPKbZo6P086ZtwjeW/RblqCK/2xFvY7Fs+4Yu9vPvs3lq841nJ8J+KQVhZB6GxWbdaXvPfzdsbrrt39gurbPq0sxYWyWulYFmc4o5VZY1VFbrP3llMJlW78G4/igNUu7Qj9njMfoJ97/CPqD523S769qe0f6sNxqmfLmjbFyrruBjv1tW/fd8lyM/lFmp65Hc+O4/Yxu+PbjPUEijnMEs4LQaE3uy/G9V6jj3DLLLHSL/Lhzy5f+Vvv6K978dOrP9+s9g9JyIMe5G9xulTeucHG3/6J9vt++5ryU494gLv8iY9rePX/g+/Z8y1+5VdOYBt7cRmKjkgyJdBADxK2NiG0jtvNeP8XdvPejy5d/lefFn3V2+6o/sszd/7pZjQvR0vu1/2VXfiOj5enn3383st+6DGOxYnjl94UcET+9D8voJLBHEVALWMqWAGRoybSpkCkpys1tU656Y4Jr75qH99sy5U/9/T6tRedOfnyRts/Kvr6d69JP3LNJ1Z/+CEP+NZlzzh/OzuCI7WQcYcecDOMLb4y1lBijWXYt5IZjeBZT96G1qPLP/Sp9PjNaH/TCXnNO7unXfe52fkP3Ckve9YFJ+Hzt+jIiGYmByvIQxCz1VcHgCjEmFmNjpJWOOuUQpuML9w6euhmtL+phPzG1fmZH/xs9cQHn7T6sudcOMb3q2Q/wnyglEKJq3ff0aOnPTcVRaDvW6owQrPDZpGcC72TTbFKN42QX72qe/4Hv7z8hLNO6S571uMewFiWyd7jKZT5FNOGXAXsn1hRIhxytWxVFCmYgCSopAZXU/mAde2mjKXfeBPw0jfYz33mlj27zjlt9KKnPRKafg9JHSYFzQXfjGgVSi+4/WJvcfP27qCiOBcovUIdia7H4bDoyqa0v9EGLv8//Qu/dPsdD33Y91SXP+P7Kyrf0kshqKM2o0hDaw7JiRpBVQ8sCLN/zIvdD0iybJSsaAADSilEi5s0tTfYzIt+5x8u+9qe+Snn7tr2kqee56j63bR5kexriIZogSD4FEizDnOGaj0QYvsJuP+oKwAvICYkICdj5CYk9mA+bQolG1oht6+MTzhpMr7syec4fFklFUewKY6EhUzrIl1nzPKUN322okpjHrULKJDMoZrQHiiOTMH0CFZ9FooIuXQA9CL0IlCELGnd2yVHWsmIdRQR+iyICVMxkCOIn1omuWElBzUymao01Hm8KcHXDbHaV1p9LbW86uop8+ShnuByxomnzI0wWYB5RsOU1XrM0jjynAscfdmDygicJ9erQIOfe/oRwOEH1fkWugpfe4hCLXmYtqZoEYrkw96ftGKUgF7QUaZ2DmJkQT3R1fhj7A9tiJAcK58z1JbAavpkRKmQktCQ6fQONNRsszHnnJL5iYtGnDKB4gRXArlAtgVwqzhqKgTs8CrMWMDqiOHJLuNxlBnoCJQC5g57v9NVOhz12BMZrq2cUmSFkBaxTdoLjgkhle/7xVB4+SVLbPdK0Q7rAydtD1AKCU+SiqCKI0Geg4xwxUEL2iTUGVMLVKNAlyAcfjxRMkaHdAGtO7CAjnqWZcKYal0dHFkgSD84FNoBIzCHOkgiuKOwQt72l3bOu68vT/7BR+e/+IkLq48fNULEj8rqypQdE6VahWgeDYUYW3w1puAQFDIUaWl1gZFALisw3kbB0zFj0QJmPc4LWsJhvzOLoCxAAGFMa4lalVFexusC2OEpkQxKBQq+EyxA9JFijmrN2Nhs/NnHv37xN/xJv9x+XkfA0SPEsqqvOmbtnCKOOC7Uc08IDjqoJAw99MZcF/gPV0xZDYGegkqL7yN1Ea56WUAyiAF6+D3gJ181J1jNsjMmVjH3HXU/500vPZ4SQf3h76984bmvzKgZfSO0so+gjtAt8qYXAs3mE/KUx5z67g/8zaz7gTPHH3nDOtducA8RbcJ2QvbUWbAciU4peEoNjoIVRbIgCiU75nVkkjLjDlaqgHOOXgqVKaqAHF5npSoSs2fBOiSukKrj8CESgSoAHP7+aI48NtqVVZyMKQUs1aQoSFjGWNx0Qp57vvsI8JHfP4JrN0RIG2ZNKoI2U3RlO1UUsvao1YhlhI6iE4pCZi+rFezsKl770uOpsvGTv7XMHSXzvP/paQlUCGbrzPC8SJQ9XPXzJ5AwXvA/dlOk59JXLpHcHoocfoov5JZ9YQcL45o/eWEDruHfXNkR6mX2+W0sHWPndEN+yDjbzAO+W2TWZDKOVKCYkNVDKrgCxpwxC0QCM5dxBqotrvM4WSC6hiqDlfXnR3GeFMqadSwgNTk2pFDI9y8f85DYdCOvCg3qoM/g3WQYM8tYGTFOnraa8vxfX6Ya91gYU6ee11/maazQakfD5LDtP+vK3Yz7ikt+q8XPdzNbPIkmwFtfLFg5bj2NR7QJz3tNT1zp+PHfcczLjErG5G6JbWkZc5uvsjZEyFe+sarQs+vUnfcqWJa6TKoN54cV0icjhAaLsCRfweUTmFdLLLd7WHIO8ZksAbJH8YMZdBgYxxHdlLl5muY4cplRYqAVh1MI66i8IAU3KwQd0RuogvgObxUWl8BtMcdwFt04dZ0H9t6bBp0OD5oEwEcCgZw9PhT++0t3sb0DX4HIAqsEKhyGI0pP0AjrmL1vuSySdULdg1WeSKYi0tIRbDvrbep9drzx8gAKJWYs7KRopNgcZHJUzN4NERKzVaG593EZ5xylRLxzQ8yKiBMPpbDIjFAHUl/hqkLNCqGbgEIXKjKg6+wDPmW8dqALlByp8gRCQounaAfUh73fPBTrUVNSHYERVQrg5yRbj877kJAbv7S8gAQ6erCKz3xxZaly2n/vaZN7lAXfdpGlRQe5gI3wvgwDnT0hL4CbUkJNi7AQFwezQgouKt4l1ov+Zt+QcsF7hti9i5AKAY9IDeXwKquWOZ05ahUOkCeZUsBn2zqhE9FcMEFVS87ROyXl0t1jK0xrRzLYr3isQCLhgsf1gFQEWfOWi5HqOeTx2mY8IzM+bPsuZzI95AZcxRyjMsE5IEFeZw9IZYG6AG0mjGygPxdUF4letk5w8azTdswAPvPVmS8217NPP2713jRYpMdKM0z0HFEfkKJ0NqepViGfQKdTKpuQ6oQvY5IzpBhZl9D1/ABpmcuI116duPGmVfpG+P7v3sa//2Fjqd6H2rbD3l6XnrlXRpoo0pAS1D6wqoXJmnY4lrjLCshEVWf3+nFkyDp45Zqhclgu1EBNg9lOTI3axgiGM4+J4WwwBNQMJJJFQAyRRM5GFGFORETITnnztR0fuaVjWca03YiPfWXGmz8cMSYIwoyWKELOhkhLMaGIYfSYCzTmMKlRMypnmBgTk2NOxiEJmTRuNq7c7FgJVFKFA1LfU3JBfIbUMS4eo+dTN9Vc+4lC01WodKh0NF3FtZ8ofPLmQKFjYiNyt4r4gpWAWAtF0XU2/K2AuxByxskL/Wmn7lj/0dvRlMgg+ArRih6lEgGD22eeP7hmL8krTzkvUKRQpPDkR3miE37/3Xu5I3pIMKocGQ/iUXE4gU3JQrgPur+lIDpjUJiKmVDhBsvJKW/90Ar/0HpO2znj4idARkgGz7gAdu2Y8vdzx5v/agUqIDukrGUYpUApIEfwiPdYY8sRYmTEQc4FVbBUwI/44OeND/ztIotF+JmLd1B3oMWjxVN38DMX72CxCH/xmQkf+rsMWg05w2bDIxJnGP9CyD2GsIBQcF6BgvMtt8zg6r/8Njkqz3x0w4N3QKlgVAJjq7AaHrxTeOajGywH3voXy/x9BOd6rCTEgyLkvOW6exdsOQkNg2RQIPZzesb86XU939jnOefkvTz18Q7NhTaCuCniprQRNGWeer7n+x6wh28u11x9XWJmI9QJloEI6rZcd++CLSehMR0SFTKEquKGz8247oYeP6m59KnbcQVEE3VldDamszF1ZajL+AI//ZQd6MT4wMcTn/+7HiMdiFca02PdvXWx5QhxZZEYDHzL7tXA/3qfEPyY5z9qzMk7pmQFrEJTwZV+eGUDqygKpx6fueTcGtcIv/2+FfZOG3CFFCJStm1YvqONLUeISMb3RqueP3jXnIjnoQ/o+MHHgsYJlQEps1r1JB/IoWIldJALwcBHx0WP9Tz0xEiWBf7oXXOigk/g9F829XuO4pBqmY9+znP91ysst7zgaTUN8+GZe5mTxPGO9ze4VNCYeecHRiQUrAUtjPOMS57WULrCR26u+PDnFfyUYx45PAJsQULg68vbeON7VrBSeM6TR+zaHqEV0ARlxFUfyrz9M3PMG+aNP/v0jDd+uEBpQFYhjjh9OzzrX49I0Xjr+3fzzel2ct6wdEcdW48QD1e9d5Xb2sBjzkhc9DAgVtAYSTwltFx7o2Ay42kP73j62T0mM669USi+pegE6ozLnh86Gx5zeuK2ueeq97S4rb9AtiAhGZZTRnzD6d9tNLGAZgoVfYFsDV6UcRlz7sN3cO7DtzMuYxxCoSGbI6MghVEqnPkQyLLEct+t96hkS2DrEWJw4TkTgnR8+JMz5loBnhSVWkENfuDMGX3l+a9XdbzijztiHXjcw+aoDXnXOWcQ6IPwkRtWqElccO5oC3b2rth6Mqpx7vfAxMOX92znpjuADFUQXJmiBpde2PCDD/ccN+44btxx0VmOn76wQQ0kFSo/JG1/9Y4ZX9o9YeID5+0aQvtbHVtOq5pLHF8Cj/7elg98seW6j4146NMdpeuxekIwwxB+7knAk+5M2bG1fzsfhxwv5/no9SOyrPDYR8KOvsYatvxRui23QooJeHjSuQ2lE/7mq8vsm2ZCrfgjGMvKatQ59k0z139xL5qF8x8B1JDL1l8hW44QkUKOcPoJcPqJwrdTxQ1fdYCiZf18Cy0FiuezNzluawNnnhx4yPEFDES3foWprUcIgrjhKM2THlFTUK75dKJkXT9HCEALMcG7bwBT4aLvG+GsgET0mCf5HIH4x1qAfwqJAXNzBHj06Y5tPvOl2yI3LQPdEQxoNL6+Cl+4bcrOGs59CCieLD2SjsJZg03GliMEjJ4GorFzMXHuaQ5vyjWfSlAdgQ3iHe/9dI9m47wzAtsnCUnQUR/zrMQjwdYjxBUqKoY8osjjz3Y0EvnYjfGIgudTlI/dWBiJ8fizHUPNJ6jwW9CmvCu2HCG9OVxOpJDBRpz1XRXbtwdir/z1F/ese/9ff2GFvvXs3Nlw5qkebEzyPb509PeDLIctN2eCAzPHj105R9XhU0SdoxTHa95R85vvavHi1nKuCqJGsoKKB6lw80VcM+X223ue95oJeWUfb/nFMb1OqGLBjsQwOIbYcoRIMQqCqxr6qARdoUseCSOKKlXqUPFDSjAGNpwXLGbknMiNp88rLDRjbl+tOG7hhCGJ0gBfOJbp1L/2lulzAX7p2ZM33t01W44QNKEloG3Ljjzl937hBEZEyILhKdIc0LMCFAoiQjFhOk/MEFw5kWlUXv5He+jagk8LiNYUdcPB0mOET35Zzl37835ESHLgIXmjLc0wn0sgZkMqUAzsoOpadud5lJQSdRGq4giN0rUNNgKkxkkmDXV7jl3Xqnrd8d56hKjSx4xJQuoRmvohYz4ImY79CXTIEHAfcq0ciMMs047HxGnL3BJtA1oB2HA8wt/36uoVb16+9HNf23fmYqhXbl0rcvbTV97+ipXYLT7swds+/6s/ufSHB1+/BQkBLw6Vinnf431NLjMER9kvsAwFY0AR0+EdQU2pl4UFHeElMyqJdjYfkqg1bEise4vOhcrXJ6R2lkauWkkAbT8e+fFS6txds7u3HCHFhr3BxQ5feZgHXB0gDzZ69HlYHQIgmDkKSgaSCxAy064wp8c04wc9R7HB5bzz5Mp9gyt+fHSgXv5zrpz/OsCbfnn0C3d3vQJ8+abb/Jdu/taxz8UHTDokQa1LtKmDGmY6JfsCAs4ygYIvNrys4AGXIZQhq6TgUUaUHtQawJFZs8qOIaYpjacpHfZEkn7uK3vH87S9mZXJ+Iab923f6JcO5zyGMhlmhqhiYhgRZH3PTK0GD33pCSyCQV0mOMBcRKiGwmdSGM5cO7L1iINCQKKQq0LV5yHUYtOBSGf4svHjCKZueCajmeTScA7fIImuq22edHK89okP7K893DW+CtqvzmYLJRcdLYQNnwuRst/+WdMTyrAJ4xEp2LoV5PZXmbOhqoOwNvgK3LOH4mZ3fpshB7W9EdjahNADZW1FjqxqzUuev/Pt613jT/uupQTs/cot+3TXKUubFlwQkSFP1/YXldEjq6koa2at2F1Kx1qRdQuYqiqmNpw/FxnUlAAU7E6zYGP9Wiv8rAcJI3IEy/8IcEC6XQ/atikNmmXskAlp9k/eD41iBYpRLJFEBn0g+zu9vtlaClguQ53gUhAx/nFpx43uI0P1OjlKIZhNt7JMjSGH3UDX3kVABKOs7ymLH2a3c3d65Pegrq8qmOyvfiqgfrCwRNcU1gbnnbm1lXc/IcT5irK2rAfvYBC8MAQN3ToBZmPw4UT2hzmG/eNIS8iWUsgk3H7/RGQoRwuwRspGkOJAqwiUMvhCwBEZLEeCTSdk1mb6nBCLAyESsOEnEIbTUetUfEMEZ9D1eTj5ZBX7Sdk/uIftkBPy2kQwM2IsLK/2ZCn4LBuO9tYO9gcoD54kZut17MhwFFZIGOyZAkUEMY+ZDFFcUdTW2weGag6qfuj3QRbMkdT5NRv2H6RCxa0ZbDpYRQhl3e8/PGKcYmurYj8hQ3Bzc3ycDbFabKzOelzev1wFZ0MRAJWArPEtaxuH2toR28O9yuCNK46QW7ITNGVMCrLmyxQME4fpoN5UKiggzCnYUN80F6IOLy2GKzKcLVnv+9d5ZSY4mWElM8o21OjSGaWqj/0KMbeqGaPzAXRGLg24jZ3ByD6R44imH34GaWrQeBkiteaosLWKomsGmAmyZloHF5hGR64TvgRCO8jSh0DShOs8EjaWCtSKZ1SW6A1k1BPThFE7obd+U3KMDqz/S1+1+op9PdusjqSSvatHpSRTk4TFiTbWt1lX1WFF0/YS3KzfZ9u3B59e2DBluUCtHtugShAxfCxMR2OyLbMt1cRk1KFF2gnz+uB+D5u9qsfMSCkRgpCysuBmTGXIbJzYCqt5jHdlw/KZh6qdEqoRbQ8uFPZpIfjR7+yYd3fsadhZzdpec1XipPKLPatveOn45feYkJ+4YnbFitefX+wDzimRFhKEUaCbJSbNnG/JNmrJhLiHztWIjLA8g9AQYqExpdONpZhHCi4HNBRUW1Isd+Zj5SWy69Y294O95LXVIoLLPaIjYmwJftAiMZWhiFqZH5Evczj0lqnSBFUl2gxcQoZnnEhxJJ8JZZFQ9tK7hpLdqx/+QD77o08Mf37eg9b/RbcDhDzr11df1Y7y5Ve/WClphPpChTBPPerHtAb/8Yrb6cuEN71sgmhi3ntGVYEUQYRlB4t5g56wU9JaXrRzkBO4CpJAjzHOa37N/k304L9V1wyyHhMll+Fzp4JYgVKteesbkE+VQgcUVOrBYFgLq2WFDhgbUOCSV66yd2GBnW1HtH2vfOCDR//w7HOqN/+rM5rb7679A6OXJXpnHmVCJULMGbJj5BoykUqMadhBUaFohzMhVAAdJVeoOmoXYYNHjy1NUTdBqwgknDRQjGg9lW8oByb4nT6OHfgEihacVJQefBj2kNx7XAVZCyob3Hs70FBT8lAVXTSvlQX0CEZThMxezG0n1QJ9z48+Zcb7P9i87Bs3LfJrty7vesnrbv3axY878W1POF2/freEjHQ0iy14G+qnqxcoCgauKI1GUl5BS0OmGuojFsBqrGY4MpDDAYvq3k/BCUgidQHqgNMeeo+rGpxNgfEQNDw4oHfQ/50pSQyphLgWjNTKkzC86Yaz31Mt+NIhvpCkwReH5Q51hpWAKXhbIFOYkqgELj5rB09/ROF9n5xzzceXXvSNWz2//X/nD/pPf7jv5qeet/Sui86WA7/udoCQVZ0viB8Nv+VjbrBqHGSdInmCZketDdEVqjwUeHFrRAxeccFcT97gA6DhyUXBV56WFgj4oBR6JE7ArwUs16438hDkk4EYVJihLBm4uCZLgGURlrSQNuirtwgLpUZcTyKiWuFiBRrXzqf0mBpFHGMd088UcwWXEk/9/hEXPhKuv7Hi3R/bd9lXvz3hNe+Z73rRH9568zMftfS2Cx8xvukAIY35tk8ZujFJ4lDISDMaAxIgJ0XFUIPOtXjXoAmyByHSWWCUGtRv9OjxEPJAEg2eYfQTDQrV0PbBruGdSms/S8bS4JQMRWgAKOz/2dKNesIL2Fo6kTICBstnkFtcGt4tEXqhV08cr+DSEnktyjDBeNJZjvPPPo63X99x9Ud50dd2n8TvvXv1uNe93953QL6CqaqSJJKrQGKInI78UKLPvNHlQqAmFI9qAVXEOkRqgofczRA3vjf9/GcDSwULGXNgcTeLbsgpVi/EOMWFCR1z/vIzFddcv0qft736xMl09/N+qPzxE0+XO1fIEKY2fDUEL2rLQ4g5K1LA+8DIoFVh72om54zLglVGIpM81AiyvPUP5x9NZDzZJtR1ItsSfQ5knyippwqZb+yDN1wz4vpvTqlk8coLT+uue/EzF/78D9buv3OFlKJF9/8NooKlKSKLaPAYhairmCnFKpp6RJmC9RACrHYFkRFyPyiBdDThPfhuiu8n1Czjo+FyjXOeaz8Fr78uMl3dc+Wpkx23PftHwlsvOKP+R5bWAULMZHC4ojHyDCtDFofTrBZB4lD2wi/QlRnZEiGMcTiwSF1ltHf0fsvlb9+ncH0k1BNWMkOEW+DmKbz5PXM++PWC0/5nL3jYjre+9OJq7+++5K73HyDEuyq1xZBQkUhIAQ2e3iJJIoExuRvjDSZ+G3NZIcoM7xZJMxAnwy+YxfvBIYyjCEWYlX1osw2KsLIU+cXXz5lG9+9OnITZk8+Wdz73/OpuK74eIETV/4lF+/mcwLzhfRi82jUy3Ax2uNvofCTGU7BqkXnuiICvAkkguHxEp87+OaMoRNtGao1tUmjbTDS55NyT/Rde8VOjv379OvcfGL4fu2L26KTh+kY6ulwYWUPUVUQyuTueHe42vrR4HJo9Z8y/ye50CtU4kvKMkMfMiYydMf3O1lg4pogtUfcNM7+HxnbwlMfOTnjBBZN141hw0ApZCrtP3tMf/7NJY+tVSNJBqYZSumGm+2QbJ80SmZbdbju4KV0BxJNdUhPRmSha7n3N3/sDRPTAlNv/gOrOyAEgO9R01s5DS0hjHbvb7zj15BPvONL2/z/S3XAnBnBQ6gAAAABJRU5ErkJggg=="/>
                        </svg>
                    </div>
                    <p>Пользоваться графическими программами</p>
                </div>
                <div class="skill-block">
                    <!--<img src="<?= get_template_directory_uri() ?>/img/4.svg"  alt="skill A-level teens">-->
                    <div class="icon-svg">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="101"
                             height="94" viewBox="0 0 101 94">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #d1dfec;
                                    }
                                </style>
                            </defs>
                            <ellipse id="Эллипс_1" data-name="Эллипс 1" class="cls-1" cx="51" cy="85" rx="51" ry="7"/>
                            <image id="Слой_3" data-name="Слой 3" y="4" width="101" height="83"
                                   xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGUAAABTCAYAAACGX8l/AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAiGElEQVR42u2dd7xdVZn3v6vscso9txJShSS0AAFCSUJCaKIoAjYGFMvMiPqqU15nxlGGcQbbKOI4OuOrjjP2gsJIkyJSJHRCSCGBFFIICaTfm1vOObut8v5xbpAkGARMCAy/fM7n5nPO3ms9a//2Wnut5/k9a3PFbDvjI19tXPyLe5vTeQ274B9/7j/4qSvcR/ZmnfLWOY0zn/TlL82eK099uS/AvohVTw9OfGJDc/zerFO+fkb5tm5XfOHk6ebul/sC7IsokGHTZuW9Wad+3yx9L3DvT19kAYsf31r1VsijJnUP7k3D9xa8tZR0W7o36xQvtYBla4Zi4bU7dHwp35uG7yl89n+G3r/wSTFFoF1BEQbYPPNB3CldX93YaojODxsvln/u/Mr395QN+sWe+NiKDWUXRjLJMi29dctW12VRDOnJh46qvzyX848DYQq8iZAKAh/nWiXGIkyRhKEoe1JXxJkdCveoDS/2xO2kmMxp6bWLAp2/GkjZGW//cvNrFaPqP/3H6J/2Vp0vuqcccfCoJrz6hq+d4cJCpiKN92adL5qU7SjyTHubS+BVSYrIYqc1Zm/W+ZJJmXxIz6tquNoZR432i6Xw7uW24zW8htfwGl7Da3gNr+E1vIZXOJau2RYvXbPtD1qEvmSH5Gt4fixdsy3OjIoBIm3TSQd27tbr/JyLx9Wbvbzhvv6z71tTmumck9576b1HiB05dA75cjd4d4i1Sus2r6owMx1SDU4eUXvk7y7Q17zcdj0fdukpK/u9vvwn2z45kKp/sTLAe//MZ2dSxEt3COxRZMIhiNAMIQ0Eqv2ykt6YXPKnI794cJfYq66T7UPX8/USeI6ecs3s9B3rG+V/ecfMiLcc3XJnee+f8+SdSdrXoJ2CQhKrGpn0XL84vfj6BzTX35svAX55x+N+wtIl+eG9/bLLSyc72m3/5ImlRa8/Uqz+Y9vyh5DxjN07f7F8TfOQOChxzhFNsK2wwe+9+L+HrH0FXhhM2WFSQ9knnHxUO7+Zm/HIE+HRP7trYP23r8ne7rT8RB40yE2Dtm2juG9h8a//92trn37Pu8ddMXWk2Pxy2L0LKQNp3G61w8sY4xWjOupgqqAdFjn8EDFYUgQBiggMoBwIh0EjAOlhT3ckj8HjkD4ECiDAA04USAK80UidQFRiQ8MQSnCNdupR89OPrC5/uh4nXHRiiSkj2vGlmLVbBXcvHvrE8id7uHOeXQ9ctU+Q8my01AIKC3gkwoMoLKDRvkoSJWhAanAUCCQCcDZFqQbQvUeNF15jBTifoYUCwIkM5SKMT1C6YMXWGr++D1b2Go6cMICqwZCRbMgdHalk1mGa/UtAIDloBARBF6ufHMB7/8wk5ru3DZ5x/+JgxgFjgzUXnK6uOqxH7NGY/W5JsSbBo2H4ogdCQWDwTpEoR9mUnjlWiQhPq3coEUMewx4NmgLW44UgkAF4i5UF0udARK5KXPdQwLUPDGCbMeVyiZvmDoKM6CgUjcQQiJRyrYLFkBMQWshtzqBqY+Fajv7Z/Wbtk0/7A+9csvWIJK9csu3JBst+LCfdvcR/++TDxdqXhRQflXCAdxaNQMiCXMA1D8C1c4YoZIw3oLTD2ARcGVWE1OI+TprZzUXT9+z4ZbSgAJS1SB/gJQjvcAJ+dgfc+ojC5oLzj4vo7IAfPeAZsBLtQ/Kkn3OmtVEGnMmJtUVIxZTxIZN6tvHUpujiG+7XF9faQ447qJ03nlDigcWWGxYMXfz9m+r5o0/4rxw5XuyRWNJuSSlEHeeqBCjwKXhNhmb2nKfpV53UMlAK8jxD6TLEGh8ZhnLNXQ/WuWh62x4l5af3DTJnTsiZJyjOPSlDEGGkZO4yuHXBegRlLn1fB0ePBkuOKvfw4JIG+3UozprRzZgKSF+gXBksUECXTvjsn3WybKNnwVJo0ymnTCkRAaNOsrRHHfzyrsF//urdmQU+t9dJUQRo6SgSSRDFOJdRVnDaUSPRiz3rdJ1IlglVSF4MgCshbUgtdkyf3rFHCQG4c27AFuH41QOCs2ZFaDdIXda4+U5wopuPvkkzeUyOtyEowUlHGk6aVCFSINIGUKEQAh86wOKVJUISZJbx+8OhIyyRiCGFuigIXM4500IWr4h4uNeOumde/9hZx3U8tVdJ8blDUKCDCC8LjC0IMZz/hgpnnWFpEx3gwGUaGZfxwmARSN+1V5b6rz9Ocv9DETNngqFOSI2QhL5+i6vkdKkurFNoBQpHhqIsADKIKiAMmhRHTIYkROAzi1CKsrNIaWnmjnKpicqqFA4GU8u4AyOe2Jh9ZMOWwR8AL5qUf/7Z5g+0i1r/310Y7+Bl2O2107IEFAgBlgBUFesqCOtoEwJDAQJk2CrGe4dCDRda7HFS3jcr4Ft/Y7hwZkFICL6gZEucfHKVxMX82296WbUFrAdsRIgHDwUhiAQDCFvFpJq+rYqhAYkIIpx0KKnwlFCRxLuYNG9SYNm0TXHPQ0MMycZ/jB43cv1LsX/BpuqU+WvEcbtc952/UKLhsJ1o5whUjjNVpALtaDllBHjfmh7r7Zw+8yeEZ9aTwbP+v2eQICnLOrgaXkKqIMBw/kzNU1sD7l7V4PYHEw57a0ChNH7YpsAWGFVizWa4dk4fyzeWUQIiLRjRGTF1guO0QwyBCok8NCmwqkygEv7rlwV9seDjM9q/cdIR4qUNXb7A6+YuX+9KShSazOU0gwCZW9IkA8C53wk6vPdIKfkjiGFeErTQJIUAZ6gEhsR5hmSMkBDIAue62K9Wx+Yl5DPT8xSjQu5cBj+9JcPaEKkbdLSFxDJm25aU+1N4ckDz1qk5I0KBaliQFlxETzXnySGH7ZQvqvFfvLL3wvlrwim5CELtwrxf+o53Xt731bhoT8cevG3dl8/v+c9dCp5ycPuC+x5rcsfCgLOOBOFa01qFeoYQAOEFfi8MUbuDSA26GpLjGGgqHl6lmL1ogExJ1qUVYp8z8/h2VADGWbR0eG1Yuj7mv2/ajBVVzppc5Z2nVGmPwDt4ejP8Zp5hxaaEW+c53jm1TCmsMpgViFAyc2rMslsl199QP/dzV9Sntquif+rUjodOHC82/kE2Z9ppF5pCulC7wpTCLEWATjMjfcnBc3iJV/V6+aUr1l8yNFT5fBFGWGsRQqCU2sFb3CJHvaykaJGSCIlCEBgLug3hMkIxSCOTnHak4qJzO6hgsF6hvCCXBV++Ah7eGPLe6QVnz5CUvAAnh4dnR7/xXPXrgk1W8vbjFEeOUvTlFu8GEVGJ2x8NaWzz3L4soz4oqbVlX/jcRe2XTiiLF6QPO+dLjW90RkXfj/+249Id2rXzgRO7hXt8yF9+8x2bH52zJppWeKe991J655xzUgBSSIcA/MvrJbYulJH2WJPKRHWG0vOxUycazps1gtTA+B6wpPjcQajACwYGQ+avatLVnXLu1BohGZlIQLUj8EiatAVVTj9c8MN5jkdWwOFjob2iGBiIiIqYaQcZSjLn+GPKXH9XzsJV4tOrV/MjYOULsT+o6ryxVVV3udme6+BD2kQOXDf8eUXg89f2v3fxCvuxGYd38bouyEVKYEI8CuFjwOCcZO3TObpW4pBxDSILKEUkKrScaAKnQ6QwHDRek93r2NAHhSiIEFRKMdpBj9JQV0yKB1kzvo2Hl9fZ1u86XqjNR7Rnj9W62wav/kNIeSWid1vYY6WnuxvwOYGMQQ+HRiU4HIE2OK3ITQNNAKHBeY0TGUIZUB5LgfAVApWS2pzMh0ghwQfo0KFI0DYhiyKsqFEvIKx0MZSktRdq8+ffX3vOHJdXDSlVWapLU8eYHJQAB6kUxCiQINGApdKpUTZg3XpDKiKUSAlc3Ao/eFCRIBeGtRtjMufROiJAAIbCaqQpkZsSA7JBj0zIEo9tanqq8dY/Vlv26Rj7C8GoEfl6h+XBx2HlRkV9C8ROkFMABdJKLIIDeuDAEZan+yR3L2oSEAID+AAKDXhwTnPdA5swLmTSeAMG8lzTKBz1DJzzVBPJ1qTEw6sEoSguGz+aP1q08lVDyuQJ4aNay8uvm5vzqV/08v4rmnzjZlpBODzCg/IaaTPOm14miA0/vNNzywKJ9e0gCgJlGEw0P/9tk7vWtjOq0uT4SQGIJlalaCSZK7hvZcqvH5f8/Y+arBpy9OzX1zv5gD+ex3jfDrK/QNwyxx+2aI09anNSjFg6xKQ4Tz72zYs66WnPcFYhVcsHISxcOS/hytmgdIn2smX8qAKPYekTKY0ioqenzD+8XjJxnAWjGEwKrA55+En4xs199JQql/Yos7WtGtXf8QZ1zZH7y9dIeT58/zd9p9+0RNxx5qSCD5xZQyIxOAIbgTIUXrNoheUXd/azeluFILBYkxGEEcePl7zr5BKju1qZUN4PYTON8yW+8AvD0iHBX71RHfyGSeIFTYH/ULyidV/1MK1W67IeVNvz3PSHnlgiYiq2v37LI1I7185AkiPzECEgEAaUIUMTAcceIph8UDdP98LWQVCqwuh2GNkB+BRrwOuYzGkCD9IYXNKLNO0ou+cSiV7Ruq9EG7pFxGCjiapobOIJopjM1rFKMVIO8eGzRjFtIuDAawfOIaTGeWh5kHIUFucVUkgEvuVHdQHCAQqchTT1KFfwaH/IF65MKOvgix+YKX/Q3ib7U+PitqqsHzNB9O8RUi6/Ljn/7mXiyleC7it0GSYtQVWSBQ3iPCJphnzpyiHW52389Tvg5PEgraNQDgeETiMt+MAgnKUQAUJItHdQOGyowYFyBYUIMApiMtJUkCc5aVjli1emrOvzGHIq0mKyLnRcXBa7TfmhB3QtffMp5VuOGfHiCXpF676MLOO1p+QsphkTuowg8ox9Xc6W5Qkb1jjc+DJKOgJaU2IpM7ARORrQKEErFCwkaIktPLkAb0C7QZqiRhhKTBaQhyHzV2Rs2togUG1MGANjK13IKGVtX3bxmo01HlhbZvkPt30OuPTFtusVrfuyMOyvGqJCDYED47hgRjcPrdrCdfNqTDo0Y8p+miKQLFkJtz6WMaY94syjobO9ZTaAl2Byj9LbMM0u+oxn09aA9nITE5epqBSpY2Yv8aAr/MnMkLdPC1HOg4+BiKEMvvprx7J1tfiWh/1hbzpeLPujkPJs7Ou6L2XBegGq1qqXCClhXBe8Z3qNa+4x/OuV8L4zQqRyXH1zk81ApejnxoWatx2leNvpLZmUtxAYx0DexbdvhjmrHJRyThgR8aYZBXEUc9sdW1i3RWFyiCsSQ4iSBotCklMqx9TiDGv6Phm0jfrOs239whX2vWt6+w487sj44Y+e1nbLiyZln9d9yZRMhCgkEQWQYEUNb+HsGRHb0ohb56V89/aMwkWUnccFjoaMCcOIO1ds4tRpJUZE4GSGE54HFuc8ulYSlRxehixen7L0qhChFA2vaMoaPdrT4wJCRyu66nOc1hQORFHgfZm5q8RUaK3y//KbT39iydq8M43aL7l1Yf71C7/RO+vvzu366nEHiL4XTMq+rvv6+QMBtz/Y5NTjA953UoQsAkxUIGRASM57Tg+ZdKDlmrs8W+oZbz0h4pf3DZFKGGEbDOoOEK0baZuDmxYarp/jyXzI8WMMs46tcctsyapGQpEndARVVA6VYIjXHVpCUAKnUCogBwIJR41vY9GKXuY/3D/lPV8anIyOSF0zOXFqfsmbD4+49t7g4/NXxvzbVX0sXOW/cszEXScEr2jd140PbKMvqHLXwowLT45QsonOyxA6sCFlMmZOCJk+LqCv3mrtL+5sw4WtWaUc1Nz30CCHjmljyRbJ7fdWCMw2PnhOlTcdGSENTH5nxGd+3GC9iKiqOufN6ObEY7vYT4ORIJ3HOoeSDoVl5qSIMaO6eXSZ+eT1C3LaIsf0yRXeOsWjdMjHT4df9RR8f37XJd+8eXMD+OILImVf1329/dgebn64wakzazgDijKEBc4bpCyBibDComVBj3asszlZWRF4//UzpvkfXXEn7/75HP1JqzJkbBFmkI+/q5up4x3SCnwAXbUh/uWjXTy9CQ4ZVSWSwwJyb3DECCUJhMfaViQ2Dg0H9BSMmRVyytSYgaZkbM23Zn4p5BGcPlWybHAr9z3ePea7s+tnfPDU6u1/MCn7uu7rvFMMZ51WQQKB83jZpGktZVXFAgQt0UfuNP0mIAw01vUS5lX3tmmVhau2FYvmLVVX/2axemNvw33+1MPaOH4CBBR4EeE9BFLSIS2dYy2iCGnJvpuQtKNLrjVtcx4lA7yHzHhCDdp6Yp3RXlYUOqRMAxtpssJjifmLGe3MX+0/dtf9m/uAHUh5Reu+EIKIBrErwAoyKsSqhrCtqa7yEdZGrXWK7KdkFJEto6U0ABM7A3f+jPihoyfUF4XW0VkJcRgKJEIYcgGYCtIqQOJ0Algy2snKYIXDSPBKgshAGgJtUV4SFJqAMmEUUXYW4StoL4niAGtSijBgXKdEqv3dw2t8z25JUaLhlI3QzhHKBGeqIFu6r9C34kceCV6ifbBDKZIQsbPua09+UChfARGAhtiD8oAcFkF4CFVCQUBIO3WRo4TECL/DCHH20R03BkH2xSuX5PxmrkEZ2ToXj9UGaCKcJqOEFYqSg4h+PBrtcwQFOQEpw8G0XELcAOXIBQirwQ5QBIoiVTgRsy3xrE2b1IJ04PgDxQ4Bsle07uv54HODDQVCQptvoEWIdJ64nO+ggJswQrh7H/ff/verk/Ca280njhgbM3YUlGjgbAWkA1FQIgCbAREZHSxfByufCOntd6ion1HdFQ4arTl4NGBKgGwlLQURuHbSJlif0N+QXHVbg25X+cePvav6rW/87Y52v6J1X8+HSIYYYWj6nH4ZMucR8EWFiRMaq3Y+9qRDxFOf/VWy+KFlgpUbUvYfFRMPL0YZlsSCwfqIVdvgylu38uhmTag78LZBW6kN86QnWpYxeXTEGVMkEzsTpC9hREs6K7TBZZolSxvMX+/5wOnRjZNG75qAtAsp75jONctX9//TzfcUn79+zr6t+3p+WGI0aZFDCUSesn+l+an3nFj56Wee4+jlj/Yf2hPCtEP3p+YbGNeSs+IdQgQUAuZvhO9cO8iWXDG+VHDCFBg/sg2bW9ZsVsxd0mT5U31s3Rbw7pOqjBvRGmlDBfWGQeqYQw/qgHkJ98zLTgYW7WzHc67uHh/y4c13bD5rzpratKIoWrovKZ1zriUOkbI1lvl9e/hyOpeiHjqtvFFV5Q6dWCy/cEblioM6W+nav320mPDrOcmbIh3lfXnYtX6AUa8/LP34h94cEw2XkbuEUJZAZqysR1z23SH6U8mpsyp8dAYo4QCHNxqhExpZiStuMTzWVzCxJnjXW0K6IodCkhlJM4e0yPjatSkrGu7rE2rqie5Yby23lZvTJ5kHTxwfbHzVRh6fD6s3efmpnwx+OfWlT5R8g1S0U9UJf3mO49iJAd5HxMbhtUSIOqmL+c6tngcWOE47XPLnb1VoV0f6GGSIF6Y11AtPr9P88HrLhrrltMMC3jLVgssoqFBYj0sFKwc8c1Z5jHUMbh1k/spOaqG49FN/ai/bt2/1PYg5y4amKuk/ce5kw4mTO8iygrHdZUZERctvb33LlS8c2gsSq5nzWJNS4HjHaSGhy/CyRiu7ooHwFQoDOjR0KDhvRoPPXqtYsMpx6jGOio5agTW/DejigPaE8ccEZF5REDNm3CA33FX+7Nzl8revGjXLC8X9q9tm5EWNkw4vccT+DY4dKxgRQK6DVmxFCpoiwyEhK/H0Oqg7GHuApqsmSGUJYSEVjlxUcA6CoBWNdRgOGFmmra1CXzbEwGAZnEcoiMNOgjKUdYgKFWXn6NCaE4+sokJYs8Uc+L+WlI3bmqMCnXHQfgm4gER6CB2hB3zLgwG+5QkOLWuT1mJ0//YYjyAmwwkQSELXmjUjHJZWQA3v2L/iEUXMQFLQWswZrBNEAoKSoRpJSl2aNA8ZXQhOmebYPKBH/K8lxRc5odIoWQIfEhDgkaQuAw8CQYkYXxRgFTWVEUYJyQAoNDkRRhpC61oejwAcAoukcBKsZTBzJMZTjQKQCoMllAYvhxOsBBg8kQLpE9ojSRQW6f9eUmRJJs6RKUANPjMNDRQtf561CG8hanktDto/AmNZvbmvdXELCEkR1g97Fxz4gtA7Ag/kJZ7eVpCbjFqllaCgiBB4MiBLPThH1hRY1ctgbJi7oM7oznj9cz7oV9S9vun2zWc/9GT7CXmeh9CK02+fEgshhpNbgpf72u7+wlNgvdIVGdRFmDJptFh6yXltVwB07hdtSzb2sXFrFwf0RAjnwEmULoAIoSIgo4miEjh6apLxIyOWrLfc9sg2zjq8B4oqaLAiBySWECkymirihkeHaDZDTjiyRKmcIXxEBBS5IiEn8JLmUIIOSwz5kLsfb+PJrcXXTz+W1c9Jype+99QljUbbZ00E1iuEEEghcWJHV4sQ+3ZHE1SRMiGzdUwWs3CluPxvvrml52t/sd9/HHNA78Jbe7u5/MqC7g5BJRzggJGdnDmrSpeo410VISKkMBg0ShneMjNm9S8zrryjyn5dnmPHWhAaZVu5nkpB4SLmrYCr781pV5bXT2lHiLwlKNAJhY3QVrDFaJTV+ESysd7GbXckVDH1c48PF+1Cypdv8OfPWZ62n3tSzFsmFwjx7FW7+p2bRez7bhbhQ4wMUNrTKErct8h/8or7+Nb/uy47+y9Pi2780Pd72bJVsCEJkNpxzwpDESvef0IEtOSqJalb2cVKc8KBcPZxcN2DnsuuLnjbFMX0IwvGdEOGpa9Xcs/DktkLE0pxwDkzYg4dIRBErWk2mgJDEFhu/K3h5icCuqI+mk0oS/XFM06o3fkTnsPNsmj14NFWqL89Y0pBkWcEQWuI2u5u2T74Sinxft8evgLRizcdSBfRSYPTp1T4yYLsYwtWhx/zucdKSyNVPN0PTw2FfO9GmLcg4byjy0RhAVqAkS1Xi4NYwAWnRXi9idvmdHH1/Y5b52ZI5clyS6gq1DNHrWo5+5QOzjoGtKeVUY0hszECGMhT7l/hGV1kl158Yfdlgym1TknfxHGtoWgXUook1YHoolx4Aq0ol1rvEmG4cGglbO7jIxcARlQQKAog74M4tZScpj+1mGAQQY22MkyKYdzIkJt7cjauT3hidcDBBwcI5TE6J/QhkAAlQgfvnxVx+qEBv1romLtSM5Qo4lCiI8+bpynOOqbGmLIDa0ElLf2cjhksEkJbYsnjBQNEnHi8XHjI/iIHdu+6d1SlVRlGxnhXoqrrYKsYBY7h4BEOBOS+ICRq/SDBi9aeWzgQ0rDHXfu2VS9iuK6C1o2jwAuHQKF8y23aUCCkhzxEhhnatxKvnG3lpeQpCO1JojIrehVHTAQQhEpSiBRBCeW335c1Ro6wfOSNgr94Q0zaBK1Bh+DIGZ4NDU/KagiVgfdU8xKKhLa2gDYkrtDPeWvv9n43LgFaYU5FAbaOkg28kgx5Tyiilpuh2B7ZElhsKz91LzgrvYJE5MPvCCkgKLDKYEROjtxhb4Xtz0IpadnowQiQup8ihy/8tI9NG1JeV3XMPFpi5UDrRCcIfIy2rRSK7cGzwA4HM4Qjrhh0VOBEgRchnrC1cCFr6dFchBMp5SrkXjF6TMy7Tmswf/6m4666I526c7t2e+VEKPEEmAICGaBxWBS/vM1yz2LPOt9PQNwSV5g6zkdoH9NeGmLq1A4+vIffHPmD2ZaHFhlOOTrk/FMC8C1vLBakKlouEs/wredACIQA513rO98EatzyYMHTAxUOGCX4q7eH1MqgRIj3FoRHUOCweC3IAOUEkQ/BS6wxSC3wQuGH+5EVFq8MmipYcAE4ApQHJ0JC2eQNB1eZv6r66RsfqbsF69zjU8bJ/u3t2m1PsRQYJwkiwKdIqWh6zZ2LNrJC5q14txfkeQ6yhojK2FgwkAvueWDPv8zujkdSVuaS2Qu2YgErBod7KUgfIJ4VmXDC4YWj9a/Va7SI8UISVQMK06Czu2BcBIFoUFAiF4rMSUAhiZEuIiJCyZBCtoY+FUgQBuuaKBzKg0ahCMmKOrkBZ4rWvjDWEWuDSsuYIiNoM2wt6OkdfJ4Y/bMR+CpStpTDTmgQlkhknDptDB22wASeHIEMI6xvttQvdU2FnFNP3LOaL4BzprR8Rm84biSh82gqFICVre1v2d5TGJ7Ce/tMKNsNN987wymHQVQOmb8w4IktBtnS6RJREHnZ2vhAghAOZSy68AjZmux4JBaNFjE4gc8teW5o5hlkCYXwNNOAwVwy5CRJ0SAIB9iSlVmy1NMTyK1nHKF2SD7a/fCVJcg2jZMBhoKAgNAXXHAinHtSG+XidwIFZOl3FPv9Wg/hPYwLZhnecrJGFQ6MQQYCRY6SloJSa4QCfvdwkTghEEJfVBT8VAXEUnraynDm0dXaTQv8Wd++ufjOxe+u0BUBOHIFty8aZNFSwcEHtnH6NOigiS7KEIDxIIRCeLAJOK0Y9FD3muXLYmRYcPjIgC5nyMkwOmJDGvPN6+pQVC8/+0R30493atfuxXi6hCABEyB1gPOgipaqvmwkaZSiiYfF1dmwwDvAmCZRkLDHN/bEUfUBBIBrSRbV8PQ9FsMzMCmHNc6tzQsEGu88pVDk7PgescGP/1ffytUDlYsuv8Z+7+NnKdo7Iq74Lfx6bgkZSOY92eSmOZITxid8+JwyyuUoGeJwWHIKJWm4kGtuH2LuioQtmWT/UhvvPxMqPSFoWLxJ8F/XNxjQ1f885/DBu84/pf2hndu1W1KaQDt2OBLvsELiQ8X2VIiAoJXyYEGpYDgVAnRQxrnysOhgT6ICAhLvUDJDSAh9qZWaEWQ8I3Pavr7yrae+s+I52/31D3f99n3/3X/e8i3qon/4Qf69CSMkizaUkVH+vtOOELM3bElGLnyqZ8a9K/2/T3os57TDh8sUEiljChxPbII5j/fTL8fx5qkJPor4wezNjN6/g97eIdYNVAlk49/eeYj+9Qff0n77c9mxi3FRZPOmD/G6H2mrbBpsTe3Yfrc9g+2S+p1fWvdyvcRuuztouP5UgJXYKCFONRWbsaFcQ0cDxGnt926p/pMPdfzysqvrhz/6mLxg/vqOntFRuvqUGX72BdPaUuCpz1xfd4tWetzwZgmpc5ScIxMBWuRkRcygGMGHT0WdeVTelarSYODau26bn4zM8lrpsJFuyttOGnHdzIni927gtgspUw+K58x+xHLrQ928eUq2w2/7+C7qu0D6Or5QIBIS3cZDD4HrK/780IOTRbs77+J3VpfcsMitHBgYjN87q33wW8/6zTcLSjJEy4BMFARKQa4wzmN1TsMIHAN/PtSItdAd21fqG4c/AA985Xns3oWUc6YHNz7+ROPvb57DV659eOdf9+1t1HdGEVQRGYRSYElQlOnuCLZ+9sLK/Oc795yj5M7PHAA6O3z/0Br7oatmN/+7t08wahSM6XA0ZMamdWXmzNO4VMo/mdX+ooeM33vv/8Mv+qavWhcc9Ozv/D6+t/3OSKQpRzJOvUWGkc9ft59fO+MIdf/bjiq9pHTrv/5e44z+fjpyG/9PbjyByChCg8xLkAf/Z8QBydpv/Vn5lhdb/itsQNp3cM29Q3pVr5+weUCMaKaiLCLL6IpeP7ZHPPX+U8v9L6Xs/w+AGrG/hLnQAwAAAABJRU5ErkJggg=="/>
                        </svg>
                    </div>
                    <p>Работать с офисными программами</p>
                </div>
                <div class="skill-block">
                    <!--<img src="<?= get_template_directory_uri() ?>/img/5.svg" alt="skill A-level teens">-->
                    <div class="icon-svg">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="102"
                             height="98" viewBox="0 0 102 98">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #d1dfec;
                                    }
                                </style>
                            </defs>
                            <ellipse id="Эллипс_1" data-name="Эллипс 1" class="cls-1" cx="58" cy="89" rx="51" ry="7"/>
                            <image id="Слой_1" data-name="Слой 1" width="98" height="89"
                                   xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABZCAYAAADFGPFgAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAw5UlEQVR42u29d7ylVXn3/V3tLnvv02bmTO8zMI1hgKEoKFFBKYoSBIMKvIlPCjEGkfAqkWhiQQkSJchDiomvHbGi0gQUQpcyDMwA0/sMw5ly2i53WeX5Yw9E9JE50yR/vNfnsz/nnH32fa9rrd+9rrXWdf2ua8M+yCe+2fzA391s/3Rfrvn/ZWQiR/rBR54L45fuTI9+bv3AvP1t7LsPDr/uA9eFT37l3v5TX+uO/0+TEQPx0yezdwrb/JBPU3nHk0Nz96ex2x/Mz9gk+dQjT3ef+Fp3/LWSj33HX3zlTeG3rMqIgLjzqWzuus2tWREV9LC/dOU2sV9AnHqi+8W0cugTb108fNdrPSCvlax/oTlj3bbhWb/5vt7bhet2Bvn5m+rnBNHz0be/1XL3nYZn1vsjgVv2VYmLTh53P3D/fx5gZ77wveLcZ7YMLypsFCnhbaJN3lFJh3q6GOjtyfoOGyfXzBgTrTtssrC/rwEeqTRdXollVPzm+3sF4ku3Dl3aN6iuOunIjPMWCbZsjvnVih2937ovvOGCN4kHX4vO1OutWuFGRU3Z/KgXJY1QMFzP2DJgsFs09yzLkeXgFy+83jYXzpTLTprrHzxptt72Wuj6m5KqjszZ5m9ZIvFqF33sa9nFa3dks2aOji7/1PtTksiyYYfmY18fQkp59YVvcd98+9Hdz70WHfrn7+1654Or9RuTWnT5sQsSTjosEBd1Nm5PWLHTsHlXxpb+Bi5UCI7rJ/bYrW880jz4vhPTh3+fen7ye40PrFgf5hTYqKZVvd/LUbEosxIVGUwRsPKoaeGp3wJizWDQjy/n2AceHj55h6v2jh8lL//7Cwu6E4eyKXi4ZxV8+e5BlO247pjpxZNnLNZ3HjvL7Px9g3Hzo+H4H92781dBR/zRqZ28fmpGisYaTUs2aPUnLN/U4tFVnjU7E2wpbpg5qrn2zGOj2087trLq96HjFd/dfvHzW2vzpNA+aooiS1wSC5tZl2qweO/lMdPyVwLxyW80P/BMX7QQYS+NXIOT5kX8+SkpcRpwaKSHIHN8iHlqVcGNvxTsrAuixF03u7tc/YWLOm78fYHwzJai85m16sjHV7UeWP+iY+qomL/545iOrA+hYnLZhQoWVbaIY83TW0p+vjTi2Y0RmvyaRYelT195jvjO70vfl+SCq/LPNLSr/fhjlY/8+vuvWCOq+Hpc1Iss1pRxwpzZFZIIcB4UyADYGISjs+YR1tJpPKHhszHdHYd8RqzcUkS/WF6e+tgac8Inv2E6vWleGvB0dXimj9OIApTpxVFibUaQETrtoNWEuWNS5p4Fj61ucusjrY8+tlpc/7/+rZw5rUNsOmamWjJvFs/N6hX+UPchM43EG7X3NWJ5f6jd+XD99CeXpYsR5RVnv7nJu4/rhFyj4gYhT1jZhM9/fQDf6v7sglmtZ992Qu2uxdPF7kOl/CPL8vE/fkScs3bQzAq4yyK3m4njIuZNrTBzvGFyB4xJIdZNbKkQSiEUWF9ShBQkRKGFcjlUYp7blfLD+wbZvjnGmZy6txgVfXHhOLnszMXx7ScsMH2Hqi/nX5t/PrMkt1wRv2JG/M7F+oGns8n//HP74azK5R88OeWMORJUSSYNl93YYlVIv3rerOz7F5+R3nmolL5/VZj643v7z147WJulYnVJp2pw4pyEkxZoxldbWFshlpbgmihtyIQjiKTdqbKkKjROFzQtGF1FBbC2IBhN0xY0mwlbhmH1dstTKwfp668RSXXtEZMHn333m3p+sHCSqh/sPv3DN1t/7IOQn74o+eqIgAC468ns8P99T/irLmMu+eyfBSYnOXcsSfjaf3kmj21++Et/3H39oQLhqh/0v2/ppu6ji8JdPmlskzcvguNmxIwyHldapEhxKuAIWCkpgwdhCGVOJCUmCKwt0FojQtsSBAHBS6QISDlELmO8c2ipsFR4cg3csaTJll2WyHDt2cclP7ngjfHvZYsu9vaBG24p3nHP6vyUxdP0pX9zXsJn/7POsh3yhs++p3Llolli6GArdPdzYfa37um/sL/s6a754UvOO7nCCTMlFR2wwuO9R6BxWhC5JpYASuBtRlWPxpcWoSSldYg4UJYRDotUDQwV8BrvS7SOcGRoPFIYGq2SKIaGN9z5lOOeZYYsa11/1ETz9G8+va8JEAB/cUN5xY7h+ufPOqmbWx/KGNOr/vbf/jS6+mAr8+177Ym3LmmeZYO/4rCxKX/1pgjV6UF6jBPIALlUBAkyNBC+CoASYF0/2vfgVROvNSWS1CkcDdApUinKIgOniYRGyRwnHSKP8UEREihEi0gogovY/EKL/7xPs6t/8JpJo+Otf3JOx1cXjhEH3VS9JCPyNb39JH+7ifw1P3nC45PA8ZPNYwdbkS/f3nzHzU+2/siH4opzjo+59JwIugYQOETIsWoYL0tilxPZOlIkOD1AwBI8xHGKVx6igkCBdppCWrSOMGWGallUSBCRx8ctcgfegk8EPrZIXxBZifUBIUpmjQv89TmemVPij64dqP3z/7558EMrd4foNQXinUfHz0yLKhudC2Bh5lTWHVQQbqu/467n1WmdqnHJn719NK8/GsrQIA41FAIVYpSvEdA4pQgyQfiAcTWUAKTFWo2QHmFraJ+ghcUEgXcSJ2OcAiks2klEaVBKIWQEziMDBCRCKIxXCC+wIqLHxHz4lJgTpwg2NIc+f93N2aWvKRAAxy8yjznVuBGZXzeuNxy07d23H6mfeP+Kyh9McOFDl50+lkXj6nQVmrHN6qHq84jFmYzQXXLeqY7jZ41h26CedPG/hCsORVsjBuLck/UTkY2LyCXF/F7ZPBiNL98yWLvtYf12F4rL339WxGGTBvAKikgyKIsDb+AAJXZQyw09VvKhkxXTJzYv2TEcej/13cZFB7utEQMBsHCCX3bERLvsYDV+492VD5ZOf/xdxwdmjy1oCY0LKdKDjV97ILy3ZJSYUS1Kr/ngGZ2MqTYuW7Z5cOFXHvAHNco4ol3ToZCfP1scfv2d4sPza/aDHzlP06kzhlFEPsXYFjYy+EPucNiLeE0pSqwzxAZSa3l+AG784SCDdF33d+frzyyeenA8Cvs0Iw6m3P5oeWYUdn0wyIRlWwK7XQ3vJMFDoTxZeO1jOlI3iaRCa1BlyVBsmT6qxamv7wTZuPSHd/efe7Da2mtg6FDJ+Gqxve9Fc/WG4UJ/+a7W5ceNq3HxOyMiB4WIMSF7LdUDIA8RXlmkcNx4R8mLO2OSTkhNSYi6WLWzftgvloeZpxwhDngX+ZrNiL89v+e7N3189N9O665v9HTQcE2E78M5RyEsXh6yLfuIxVDQXVdYn9I3FLM2D6zdHrG6zxLVG2SRufz+BxsnH4y2XjMgAG5fZedv3F2b1q2HufAPcjpkL5l0JCi8KCBohCswISBdQRCawmu8CihamFwRVIlHI0SODZ6qyGh6jaLACo2XEkeJw+EChAAah3Qt4gJybXHaIX2J1yVN4RBotBcIHzGYtjAUnLLII3FMGF1+7PJTar2nLJanjZLy06bTHhQb+prO/R/cOniuch2Xv+t1Kb29XfRnjqqMcB6iYCiFRcSC0howOUqAycCokqaWKBUYGDLct7LJ0s3QrYf5m9MqVAzgBBEtlEwoidjS32R1X8qz6zSbdxZ0jY656GTNGJOREAi2Ax+ganJwdbyKEd6QyhrB1lkwtUZv2mRop+s4dqHaCdy153VQ5DUD4nPf73/fkvVJx9GTDG85aogQFC3h6ZAOQsRwqLOtL+a7jwRiNcCHz6mh8zoyVVhXIcvgnqd3c/8zHfSbEmENrbQCSYpvNpBJTF+rwdLVjodXCLYNRhSFQ+OJE82uLUPccEuVi99hqHVYSgFSW0zwBGpkwmJtRqoilFCMrsKkHlg1rJNHl4Xxr1soth/M8XhNgHh8tRvzpVuHFtRUefkfviUiFBajAxXjKH1E6YZ45oUK37/d8YKyzO/uwmYeITzWGR5c+iJ3LxlHf1kFLxjXKSh25Cya30UrL+hrVLnvsYxHnu+iVANQeqZ1VZk/x3P0kTlpNeWB/zLc9XzK1+/YzZ+fO4qxlTqujCi8Ad/C+5QoURQ+I3gw0nLYlISVLxaXP7Mpvxv4nUCs3TokC+OjeWO7s//RQPzs8cF3Zpn6+NuP6WBUx2582YEtNImBXJU8v63Ct35Wp2UqRMqycEpGqj3P98d87xcttrzQRdk1zAmTDe9/m+ZHD1dYtl0xrub4+dMNbn/GQ0Mg4iYnzejkbQsl8yY5oqQADASYf3oXg62MX201fPe/Cj50Zg3sLmQ0ChUkQu3GF6PQAgoTIVzOjDEVyrJk86Ce/Gr9y4ZsUsRxBPzPBWLZFl/7h28NLOjtTnjDUQ2Q3RgjcRaKUrE9a/GdewOZMUhfJ/IdTJxtuP2Bgp8tl+xOFAsnK859U8wJUwNSNFjzQk4jHsU9TzYYasbI1HPaUY5TT4oY3yVJggUPIaQ4H9AioKLAxW83rPo6PLfe88zKFifO7iInMJgXJPEoIt1E5i0inYJ1jEszUiEZHJbdr9ZHlabeFW6fxnafPnz7Y2H+mceLA+Ix3bes9aYQ0ssWzo8wFYm1BbJIkElBriLuus+wpZ5TMy1k1EM57Lnj/iH6dtUoRcl7F3fwnhOhVgKiQVmmeBdRjx2ZjTi2N+e8M7qZMQkSSkKAhtXEBhQlCI+VMQXQU7Wcc7LmG78sufvxFoumaaySfO/eJkIVnHVKJxMjjw0ghCKOJEE4mi1R+c1+rd3WkgMt1+1xsl4KolgWT6/Z3allsAtmjt6rb27E29dnd+SVH9/Rf/aDq8vJI73m/ybL1tkjU61Yt3Y79z+mUblERg2wgXVbAg+vCiRxBzIbTchzirTFukFHV+L4hwsDF55QUmMYNFhbw5jA0G5Hmgv+YO4Ql/1pN4dPgMQB1iC8pWpyVBhGWNAhRpdQoQAZ8eajNLVqi3WDXazYCEtX5DyxtYP71km++oOMoTylIMcbQ05E0wtEqn7b+eIypHBeG2k1wao8s0ElsnQyWrt5cK/jPGIgduyIxu5M46tu/Jn4yyXr3aj9BWJqb7Ep961rV+wee+MPntrNB3/qeWqLxpuYWx4LkDaJWp4F8yy5kmgHx45xfOL9liMnCLyBlugAYdG6RQPN7ImeBVMy/uJt46hRUspGe2w0WCwEgwgd+GAIymN1RkABJWkOf3h8QIdh7lwjWfKCIRSBVHpWD0X8y30SRElSaEQOQSriuP+3PJKzpvT4o2d2DRw9tWNAiJYvTRIdNaMycNRhPQOzpnTt1Ws2YtO0o58xxjdphPDxG271+XObw7Xzp4h9dof/3Xt6vwV866n1efc9D6X3PrrZvf47t1QvW330ANtfUFCrYoXmDxYEGoMGpepc9J4xTDBADlJDUBkZIDxURc6V763gFFSEpSw9wkicAIJHqwR8wDmP1AIBKBLwkMuANk1OPnI0P/jFTtZvCHTHntR7/tfbanzzzq2s31LjvqUpb10Iu/odWheM7up+VQ6XlsGWNt8n18CIgdg4UJ8exzHdUcn2Un7qyz+2GXDNvgLxkhw9Ix4AfvDNX4btty0ZLu5ZIa6I05yiyOmMCo6fLXjj9Aj0nslXAJFFO420CVI5RFDgHbEMeFrgEwyaos2Ha9M2BHgEyjgg4B3gJVJA7ARQoVvDnHkxT6zJ2d0U1LTk6LmOVE/hmp/Wue3xgpmTNS82wVtHb0d4VSAWzBzdXLt5cMQ7JtgH07SlP56cycBfvqOb4yd4NhZi2of+vf/y/QXiJbnwLeLBkxbFDxXO3pAXNaKimyOmpcQkWCOxoknLQa48iIDAogAbsvYga0+hSoKMQOUgLZELIHNA4FwAGQgACCQKqQDlKUy9jZiFY2dpROEIQjNzoiHWgqNmw4SelJ2l4r4lOc9tt+iy9sXDJ4W98mZHYo72C4idg/XRjYZidmfg0nOrLJroPrh1oGvSZ78eLjhQMP76rfGtJ46rPNJTUZ/QUjFrrAShUKVBeE2ic7RyOG9wssDqkiBTMgklHgOoICgsOKUJRmBde8sKloClDAG8BiFBFFgxTETSVkA1WTAxIciUumnxlmMCkfcYXbJ4tqJWhSdWlizbCFVB823z4oNOYB4xEKHVI8ekmlp3QdUaLj87Ymr3jkuXbh9a9Imbyg8cqCIfv6jynWnjzSafDDBtIuCaCA1KJDirEM4QZJMgK2gMUS5JfNu8iBDhg0KTIoE8ZGhRBQ9Cth9M6U27IZ9TioClA5zHeWhQYWynYOGYgqMmGI6drbEiwhGYPzGDvIGVFUqXcNjUfPXBBmHEQKzaHqLcNqPxowUtG4GCbhO4+Pyx1DrV5Uu2DS2+/o7ynQeqzI7B4TF5GZg8SkFIyQTkAbQCKWD7cIXPf6/Bn1zXx6d+2uLFQQcqw+EpASkCghwp6u3ZoEEIiScgwks9zvHEbB+QfPZHEX95wxBf+nE//cOBT18U85lzKkSqxIs2s2Pm2ARnNV61MCK/+viF9qBTiUYMxIv95Vgn7eWdYwoiHaCALa2Stc9kdNoMJZMP3ru8fPN/3BEOKI67sxH3RjKmM7GgFElwxBLwjhDgB7c2eHyzZLvoZfkGuOEXgSYVVPDEJUBBK8RYxoCj/QIEBiloE5mkp2Xh9jt2sWTTMNttzJJNFb7yk904odFRiQ+KuPQYJBUNQqRUU331zIl2/ekLu1e8ZkBs220magm9seauJZJP39bko/8SuOFx2NDqpKcoaMlwaTIq7NNO4TdFWOOdC6TaY4UH4WniIYBQ8PhWT3ch+afzwEnPxnUlEg2FxMbglCUNUKG9YUKACAbtBag6TknIaqQantjagXGSz1wUk+SBJ/r2bCBzz5AMYBwWiNKCgIUs4XPvr/37oQABRrh93fAC05sucM9TFq88tggE6egsDb1pzqTRkv7NCX0virEHooz1pZZSorWm8AGcJjWCQsV4C8fM0ty3RnHld14kF+M5dsYggQgiBdAOEDkQWITRIDzBe4TV+Ei1F2oDBMecaRkPbujkE99+kaBGc9JMTx4g1jE1gELilCNWCqUUPrhDGkQbERAr19TnVlNDqFuQu5jXo1h0xCiOPxJqBgqbs/WmBis2hP1K+31JjBbW+UBReJQSYALC5xiZIAS8940pZWiwalOF2ZNK3nNaFwkN2l4lUF4jJQQc5Z6cEw3teR9inAAlBLFo8e7TOnE/L1m1NWXBzJz3v6HankkKBBmoCI2gKANCBLTkkLIZRgRErZrUd+Z1TprXzUVnwhgBOsrZNahR1qC94vBxggeep+dAlOnpoH/bQKBvwDK5N6FwEMkE0SpBCyaOKbns7ArCC7xuOymEdwShEFi0VGBBKElAIZEI2d7GSiGxBAICrGd6J1x6niJxnUgfwLTARQTnEVGCc6AMbN+d472np5v+36X3+z6z66pm2lFJXDMLcSqzjMSovEAbaAzjq1I6H+m48JnzWqsotS3ZV5mY+a3/duWkq0cMxFlv1T/955+LGQOt8KFxsYDC47B0RDH9eYYWltcdFfHL9YPR7Y+H+Wcet38e2t7Osm/bbsELQ5LJvaBUi9KDMQanJRkpVVECBZYK2pd4GyNN2+IIHN4JhAw4KXABVAA8eDwaEELgfYK2FqsVMjRBaHJSlPJIIShckygSEFK2DbYPgeO63e+kmdbTzlo9cZfopsW7DKcdhiotN8ikFHb5LrIsJ00ENii09ARXZcBXXvZMjAiIt84Va869dke2dVdOs0ioRBJBRCRAeYUShsndCm3LSx5ZUzwN7BcQk0eVW5ZviNmwO+LYmSDLmBC1V92mLzAywnuD1B7tRNuZp9lDRAvtPL9IAFF7KRAg0AQTAAcOPBKhDXiIPaAVzkYIIPeOWBo0ilAEhIENuyNk8EweVWz5XXpX81az2zm++tHRuDKgjECW0DS9UMJFX4TejkE+8vaEYBtY3cm1P/K42q6X150RL0CH96rVreGC1ZsdIoDMDY0EdOwojUJZOGJ0Dxt36mn7AwLA3CnpChHUNc9vLikCCOGBgjwv6JCaxOZID97H7XNCACcDUoLC4JDY4MGDCgG553crHIH2Z6QEJyHQdhriY5SAqMyoCoXLSwQFQXusgBVbLCKoaw6f9LtP03lqo2FfIIFIC6xvIVQgBqoEhIBGM0cSUY1Ho0IdicEp9fJEGDEQc+f2rEBUeXZroCnAxSUGTzUP4AKRLlk8J6Uva45dvjmr7Q8QJx0eb8MLVm8bwocAaCya2BgoJEHGIMHJgtyUeA0lLQjlnjODQsh2KFQIh8K2f0cCuv0ZUVLSICho6YJSFlgFQSVQSGJtKFFYobEOVm8bAi94w5zkd86I0lUiH3e2jy6+JJYphCaaDFSBlRYRpQQkrXwAE0ksDtcave8z4oTJPGqj7Lol6zWVUOAxRNZRdKVgBQ7DvFktKiUXP7Y5Pn5/Z8XMCdl6W3SzbH0BEgyBXGbtU3Iocd5iQoRxtJkXlJQy4A3oYBEeCrXHwyQiCtW2vzqUhAgKBApHKSDxbRMrrUMECwZy0SICIgdPrCtolilTJuabX01n44pCO0WgwCoNAbyqEEJCEBpJifMJkWsitSBYQwgKLRsv78RGDMScSaLo0G7ohReb9NXbjjbwGNl2P8gAsdGMqnhWb2zM3l8gXreo9mgoi2vufroJyhNsRkyKEwVBGJTWICwoTVxkxHRx1TcVH//RMA3fTspXwqOtQZfttC4KSSYiPvq93fzjTYqIGnGRYaVCB4tUCis1PuTEIaX0OUjPQ0+10Hm45sRF1VctGyGEwBQeUMRBgHdIu29s9n06pMyfnD5f+ITntgsoM4IUKAFaOQQebQTzpsSs3lIetr9AnH2UWNplWoNPbq2xYXtAixowhCeikAEfmngXsAi8aZPZdwwqlq13rN/e2mOi9vz0rv17gNWbC57bHNi+WwASr3V7K1t4fGhiRQBiEENoUWVDX2DZpgqjld317kViyavpHHSTrOK54Oo677q6zjlfzvjYD+NDB8TRs8wSlPziE6ssaAFSIgJERiB8QOI55vCYlq1UHl8fxuwvGG86qnpfVpp/veVRaEoPwaA9CARSOoR3SNpnA28Db1wAsU+59ieBrz7UYu1ASiuGVqxY0x/xtUcc19xSkoYaJ80FfEDIgAGc8kjpMIh2VA9DHjw/eDQwaNUNJy/quH9v+hpXs95n1KIO0C2Eb6Fc2Ntlr5B9YnGcdrRY9Z5/rNvnNwdciJF7yiEZHciLgHCBGeMhNXzwqXXZT9hPSuL7Tk0e/tMv97/lodWG122OeN3kdhK7Kz2YBKEFwmd4EqQMnP0m2J7l3LVMc/tyyZ2P9uFNTAgS43PKtEZeCt48J+PcU2JwgRAMMuRtU4fC5Z4okhASnt5seWBtTk9nPnD+aWMe3Zu+UnmfeMWNH5Yo04u2gPaEMPL0k332n0zozF5oNCPW7oTg23t1KWm7AYIiNjChK+O5DeX8/QHhJXnXsZVbrCyu+97P6mzNBEGWpKYFGLzTKJHgJYAlEbv5szNiPn9BhdNnOGaP66GW9FCLu5g1tpszZhVc9f6UvzgrJaIOwuEFIGLINGAwcQtEybZc8J07GqhQXnfW66o/G4mullIXeDS+HaIVJTbsWzh/nwlm86fHz72w3PLMxojDRwVKCUZKUB5danLZZNF0xd1PywkHAsRZr4+Xf+a7u59csTl87is/dR+/5HzFKCK8B6EBB0I0IRjwo6hIz6IJcMTpFTC2vVQEUEKjfHfblU4ENgJpwQ9D6CDEgeAFUkYMIPn3nzh2NcPnjp3Ms+9ZnI4o9uBdj4zEEM7IdnK9kChRg32wTvs8IxbMrj2Ha17z/KYchMQGQAaUUm2fjgrMn1GlsGn05PLd+027AfjE+aO+FVWq+dJtcMudLVxuECLg8CALSlkhCAO+QNhACJAbgJzIZ0RhGMhp0KbBiCID2SCgsbKjXVtEeIRoX/j9O1ss3cb11ajSuPK9o0ZcQqipBisqSByByEm08IRwCHdNAG84TGyxKtUrNxQME0gdOC/RKbQiS5xXmdFjSczwpXdvrL7tQIAA+Nvz4qu78vzTt69R/PO9kIs62pY44Yi8bYdCZYRD4SRElAifgkvAd6BCSkUEBGWb9io1pSzQBHCK1AaEE/zbL4a4ZYWm2w8N/PtfJ5/bFx3TUjYLEaMpyZUADAhzaIEAmDFerS+JWb9NvJwOKfFoAV6BxDFromb9lsb0AwXi8LGiuPxPK/+UZMXnHlre4gs3x2wcjlEuRSKxTpJJT9ANNA102Q6ZBpOT67xtytokJ4KIyEOMsBE6FCAlO5zmk9/dwW0rYExQn/7Y+3r+cV91PBjniP0iIR8zMyxZvd3w1Bo4ciIIAZpAIiQNEZDCceQszbd/KQ5onXhJjhwrhlbtCJ+6/nsD/c9sFuOu+kZ0+flvtZw0TxFJ0EGCrUKAIMEHCCgiUeKlxwqJDBGuLEmUbe8u8pifr2ty8z2e3aF2/bSq2vzXf2iunzNO7HNecdBNsqiDC66ukwWFSgWHTahy9XtGfo/9mhGLpotnfHA3LNvQ5hohHDIEIqVw0iNFxJxJGlSFO544sGDRS3J4ryhu+Kuea0+cFT/SF+rXX3/7AJ/8/3Zz9/MwFADjwViELlHkaB8Q1qO8QAdHEDnawLAX3Puc4spvwn/c0aTfyetOnBA9dMPF8bX7AwK8BueIl+SISWn9ousbuzfvVuxoGHo6PBKPaTO/0EIzrgajO8IlT67IHgcOWsD9snPSH134xfrRu+Uo1gxnrL4t46a7Yd5UxTEzDdN7oaMrUKkE8AFZCF4YUDzX51m5ybB6Yz8D1jLoEzpE5xcvPj36yunzxQHpdzDOEfudH7Fggn/24XWw4gXF6zv2TCzVNlEutNeJw8cVrFqnZh4sEF4Slw3LnqTKO19fYelzg2x4IfDEuiq/2ljHlRk6aLRIETLCtVoQFThTITSgk5SpEw3bhwO06tnp8+MDfkgspS4R7dpPyD3niBzFyJ3Q+x0QXzwzWiKlvG7pmvYihZcECakGF8AHx7GzYupWVJe+ELoPFggrXghJpruTcR3DnDbX8pFzBH/7/8A5J5XMnxIxbmw3tVqVYDMa8SC65pkyPub4GZYL3zDMZ/6kxV/9oWLq6BZWdOinN9nOA9XJux4ZIXFGUmKxL50j9kH2e0acujhec84Xmn7VpgLnInRQODypDrSkQyrFvEkKR+ujaze3bgaW7G9bvy6/ep7jszi67Jgj67R8jNadjE3hzLmB848VbRc0AaEr7fRgK/CypM9muKKDqisoBCyaEvPE9uKjKzYmPwQOiDTWVIOVbqdwBFInQTp8cAhGTgg/IIpIT4fv3zEM23YB2pMBSkZENhCCI9KeaROaLFmfHnMwQAB4Yk3z2GpmOXHiGKRQUNLuroFMQaE9SIfwBi9C+5QtDbHvIHGB4EAQOGouVJ3mkedaB1y5P3Yma2lIy6ztPgmCXOxbwv4rgLjqO433XfHt7OKRXjxvanWFF5q1W3MIDonDBzBGE4LAesdhE2PWbRycNdJ7vprc+nR+xLYsmtTbW5L2NDBegXSUYiepzkkCRLZA4kGWbXttc0wYIlYOKPDKoSWMiWHiOM+6YT/jR0uHD+hBcVpo4wy5CSAMQmjSfbzHK4B4fEt+3LKtauFILz52Fk8UnuuXrMkJSGIEXjiipO0QVNJw3GE1LFo/vtrut1scYNm2ULv5l/kf6aEtl/3ZWxKM0O1Cu7SIEoOUCc6D0xpERIZpE5FElSA7KVAEJVFRSfAlGsGf/4FGSnnJ9x9Q563auP9l4hJrM6ciLvhSwdlfKnnPFwuuvGnfita8Yo1IdJFFcuR76TfNERvO/ad6sXKbYCgXdCUBJxxSK4RQCAvjO0q6KvryJ9Y3fwHsV43Y235l5//jt3nvAJW/+6M3Ko7oVexslkhtkMaxo6zxk4fg4ZVNduYOkSmckQhK0iKlZSyjap7jDjO88agOJiTQKkpmdivOOzrmpsdaV1z545b+/hP5zecdGz+xr/rJ0OHzokVNaFw5TNXUCFT26R76qu803vf4lvy4RBdZM08qsfLFhdcMfqaQKlo0vvL0xy+Qr+r8mtqrNm/Yati8Q9I1qc0p8gG0bKdJGVEwszdlzfZon8KnS1e3uv9rbXLykrU7F//Hf+WdFVdeet6JFd68uMJA2UJHKdIHrDXct8xx12PD5ELhEkksQWKw0lEqKAPsHnLc92SJUDXeeZREakUz1DnzOINKNT/8lb38W78Q0Z9c13fmUXOqSxdMrzx36jyxZiS6upDLVBV85fIqkgpJIbER++R91cNBdZZ0RJEURax8YWnpuvA1bxNp1cBed1ULpqrlG7YLlm8KzJ0o0Ag8nthImpklyMDCaZKv/XJoxu+6x1NrfPfa7WL2+h1MX9fnZ77Y3xj3qVuQZTJwqRxOOOZwePebupiiMlTIaakAOcQqQ5Hy6NM5IQSuPL/KhLEtKi5gHZTakLocrzWb+lK+cPMwjy8veeeCiCiylMGQeXjzXMUJ0yUPPRtd8sCyhIeWlTy8tHnNH33GM2aU2zF9kt0we5JaM2u8WXfklN+udSu984gaSVCUuDb5eZ/mA+ir35/860t/XHjN4Gfqwtd+/P/2fATgJyO4weIZZsndTzavXrZOX/Gu4yO0ku2MTxMhW5oQNDOnOEzki0dWhvGvn9OuYXHXysbhtz2qztzRn/V++sd5xYbkUiE9URimd5RjzqwOTp4qmDYmxgUHJqNVBjRVSuuoRqCCoVKDXblBRgnTuyUmU6AssS5RIkGGBsYVTOzuoqVr5Lmhq2pp2JIyGCIZt4uh1Jqcvlhx5jGK3blly47oo7/aIli/tcH9axz3re1BlNxw/j8O1edPC88fPb9zyVlHiuUAzXiokvoIWY4iVi3QNXJbkKqRLzuveOILqSJvk33a0h41TQxc8MVddv02Q+4iUhUQEpSEKIDFMKonkNb0FY+taz0C/PTvvzP8x1/+SVgcEX8oETmvn+1ZNKNg+ljPmGoFX+QYU+4hE2eY4ImzGC8UdVcg4wKcQXbECFr4MkVKTyJKgorIRISykhACmC5cCToWuKgkWEtSdWStGpUCTF6nFVcogqfTDVMIRdIVOMKkHDG5RRCKUhh2DDmeWeU+9MTKwNPrFUu2hOv+/qvZ05/6QPI1oUYhs4gs8igiygAVIffFMr0SiEXjK09bNaBHMhN+XQ6fWFv9eF+T1Wth4QK3h/AIZQRJociAhTMjHns+O+ED/1Y/avmGuGv8mPChvzllkO6eTpRq1/FWWuOFwJsYJyRuz4Eo4GnqHBEgEYJUtguWyAAFkmAAq2jIEhkPgwuorAcbOTIkQveR+F50FoEREBRpaglC43QVaUuiIqIlIqRURDl4FXCiIBVdqJbnsFrB1MV1Tn1dB1t2xNx2945LV+yqXX3ejf4LrjlUNNNBkqKXIDxGBepKU92XNeLX/9jbwvy75Mip0TO/2tq48en1xQePXRBhwx6ShynwQVBpehZNyXhoWfrxor/g1LlN3nFWBTVYw+yhxEglECIgvAfn8FIiRft/QgYSFaO1JNICuWfOOg+RNPSq3TTyhC3bDFMmVNgT8yHOMpSp0Ay9rN+lUTKjogtwnUil0QmAILIRpfN4D9a7dvsOtOqg3MMuL2RC4UqCLZk+IebCc3q5/6n8il883k9TjiIuB8kUJEJBsETO7NNx+aAURTn7RLH0rGuH/dKtFuEjpKgjUNSilD5f0hnHzBgTEYkmc6bXeN9bINQblCYlOItz7RoZQigQHm3aySFGtfMdlAK9p1OvWAQlEDzHz9H8crniuluGCMrQCilCZYhQQwpL6Qq0zUHBGcdVwdcxWgFt7pHRoLUkEHDB4FzbrBV5icXhjSELgqBSVAjQqNNrAm87roNxowNfv3eYXBne/eUdVHwFX3pmHC75wtnq9wsEwIQO+cKWhmLzThg3JgUUJgOZa3RkqUrF1HEBHcDqgC8jRLAEHEoHItMuEy2lRMr2Uy9DO+j06wCEsCdjWgS8kyg1zAWn9SC044GnYkpVQ5Q5sfPkrkDJJiI2OGU4amaDdxxfhRATgmmHT0WbcCbwiD331bo9gKl56dDoaOQloYxRIcKXbRd7GgWOnpnw1Vs9No6II0lZWmKZYFqWPWn3v18gDpuoVm1dEVi5cYgpYxVFiFApGGfYOgyb+gSFNzzXV/Dzp5q89YgqAktkDHFiUPK3Z/JLIHjvQQiEaJuvPYhglMf5HqreceEpOdI47r9/mPNO6eCM18dUsBS+hzseb/Ht+3LGjuqhQ7UgGAoBcTB7aOG/1uavte8sSGWIhSKONSHWZJmj5SHzEuV3IEQXplKikHzjkgTlQGkPXu//Yn0gsnCmW/7oKv3F57eZyxYMpuwYzHl0g2XzloLVfZbSBYSz5LLCs9u6OWM+RCkYo142O4H2kybEK+2QlL9tbAO0v6BDAl4RUWHD1gaZiZk8vkWExCIxwjG1N8IKzeateZsZLuSe1sRvDf6vi9LthkIhEVoiBJgIQhTRykqKbAwqsRTWkXlP5Gyb6+MEQbh9Gt6DBsTpC6orzr2m6e97DpZuguZQCxenqAJcRTHvcM9hXTE/fVCyepPFpcMkSQ9iz+EnBAchtF0je0bGOYeUEiH+e6hCaM8MhCeEgPSibQE8bNoG3jnmj07RATIBWnimj4aKMGzfLqDUOBeIk3a5h1c7eAUKhNAI8/KTghSKWICMNHXXdrtjq0RJC1SFloA0CKzw6P3dNR2oVFRo5kKxy+YcsaDGvImKEyZapkwwxIAXTR5aannRBoYGK4xOXxrctsZSyjZ10LcXB6XUK/7fRk0QgMAeUxUAUbJlN9QV1EZbOjoM1st26NaV9HR5Qpdme8OxrW6Z2J1AKED8t/kIOORvwCKIcMEThEPsOS63i60ElBJEUYsXmxqZxDjXIIghdNkJKseLQ0hC3pvM6SlX5lGLow6P+ey7Au89LmPaZINW4FWBlxUOmw5FWbJ8a7zHF1NHICiFwjG0h8Ut2tT7lwbkpfUBXn5JBD749vcGYVj3YkHVBub2VhkSHq1abe+rBLxm/tgCvGPldkUQDXIR/8b91J4P//oLlJBoFKpdTqX9vminCoc4ItESYVt4243wnRgHSENcgBUtvFCUpkQWGfgUR0RiNS1p0UHte37ESOToxZ03iYI/2bChpFE4pEjBthmOstToJhw7uUq36eCZdUME4YGY4KEdV2t/VZkXew5q+Fd9KVG2bbiDrbtiMhuY1gsVNJRp++l1MTjNrN6EhJQXXzQIVyX2aq/332v7VlPtkPioQWwGyCUQN8mRZNEwhhiZQ5JXqOgEoTKcb2BkScV1UDr7sg/koJqmMxfJ8P5/2t3cPRTYuMswfxSYGIKwWHK0FkyZEZPfm7Gsz5PbQCJMm9ofZJsUHEBIB6j/rp/xu0RIhPR4K1nflyNVyZxxLYTToAyCHKdjJILJEySZG2DVjiooTQihXe/pACVyEuFbZGYsf3h1i0QXIBzzpqRcda6k0J4yHaKwJaVJUFrQzOqUxhOH6OUZcdCrXM6fHq96fBM8sUIy983gRUkRBIgULWByj2diRdDXL3lms2DqmAYxbdtfeo90ChGX5EgS++pOs1w5YmVxNmLlNoMPhsMmRqA8TSwVG2M1WFrMHpdSocrGLZ7BQQfatA+SByBeKVwpsaGbwhl6jEOEJh6DtBpsIDISUQiE7yDYGGt3k9BJFBQulC+P/0EHYuYkv+bRdfmfLV/vv+LekqBxVIqk3ZJ3aJMxa1qNgfowK9dJJo5P0L6FLxOkEigJKmhsabF7cSYnZYILTYZyR6u0dCQlXZGE4KgIBb5EYNAYxlShszvjxaGcrZmmK00x/sC6b2WTODZUSk+ktnPTRyYSxGjyPQ7PIC1FkEROg1JYqQiyk9wVYB1eqe+/dK+DXl/iva/rqKdeFpsH6wz1Q0nAR7BmF9z2pOJffqJ4fK1nSJZs2DCEcx4VFM6J9obJB3whUN60v7zp1V6mnTS4ZoekRcn4qSl5pLG49oEqUu39QMujfcn40Q4jErbvTDBe7/3+e3lJPKWFFimtZBRBeMowiBENpNvD+BMKfJUib5DYFt0+RlmLlBJr7cvjf0gK8E7pTfrW7Mr59kMlQ5S8uF6woy7wOsI7j4szpowexYxx/cQWtIhJtKQMBaFNUcNI045DvIpksgUhpb8PxjnLrGrUzosQFZSDTEpKAp2JIAjFtFGdbFvl2LJFMn+KJtpH6vxvSqXspF4GjGogc4dXCZHvosjbSe/5njh6IhvEsQPZogiO0sdIMYzUnUcAD8Ih+mqb6+50R/zy2WJZVCjKjgY0IoTxiFBgbA9NWSf2JcOmi86QQVMgqjDsM4xMMRQEF+1VOasKKqGkkdcQiaFTFkSqoBQtYjsKFZo0EklcT0EVNIyiaCZ4OUQkc6zfr3TwlyVX0Js5dlYSpMvbfFeV4NQwlZahGQl6sjrDSZUhV2O0GmDQx/SKlO1qmNSbv/zp36T/CodoRkzvlZvGlkPv2qU7xspWO0kdp4CUUmYYNF5qqq7AISEBHKSk4Ghn9rD3kK9yETkR2gRwBQ0HjTICIuo4IIY6NLDtcgMugGwhMJR+3/IX/m8SOSEHjcaUexx8e4pz6VCThQGDt8NxF8EHajTJXUQskUOi4atB0W37XqZ6/B876II3h2/d2QAAAABJRU5ErkJggg=="/>
                        </svg>
                    </div>
                    <p>Писать программы</p>
                </div>
            </div>
        </div>
    </section>

<?php $args = array(
    'offset' => 0,
    'post_type' => 'teachers',
    'posts_per_page' => -1); ?>
<?php $post_teachers = new WP_query($args); ?>
<?php if ($post_teachers->have_posts()) : ?>
    <section class="teachers-teens mobile-hidden">
        <div class="wrapper">

            <h2 class="double-title">наши преподы</h2>
            <div class="row">
                <?php while ($post_teachers->have_posts()) : $post_teachers->the_post(); ?>

                    <div class="teacher">
                        <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?= get_the_title() ?>">
                        <h3><?= get_the_title() ?></h3>
                        <p><?= get_the_content() ?></p>
                    </div>

                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

            </div>


            <div class="teachers-btns">
                <?php $teachers_page = get_post(18) ?>
                <a class="link" href="<?= $teachers_page->guid ?>"> <i>все преподы</i></a>
                <button id='invite_teacher'>стать преподом</button>
            </div>
        </div>
    </section>
<?php endif; ?>

    <!-- <section class="news">
        <div class="wrapper mobile-hidden">
            <h2 class="double-title">новости</h2>
            <div class="carusel-news" id="carusel_news_desc">
                <div class="news-stack">
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                </div>
                <div class="news-stack">
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                    <div class="news-block">
                        <div class="news-date">
                            <span>03</span>
                            <span>декабря</span>
                        </div>
                        <div class="news-description">
                            <h3>Киберспорт - развлечение или новая современная профессия?</h3>
                            <p>
                                В последние годы во всем мире все больше набирает популярность такое направление
                                как киберспорт (e-Sports).
                            </p>
                            <a href="#" class="news-link-arrow">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="news-img">
                            <img src="<?= get_template_directory_uri() ?>/img/desain.jpg" alt="A-level news">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper mobile-visible">
            <div class="carusel-news" id="carusel_news_mob">
                <div class="news-stack">
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                </div>
                <div class="news-stack">
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                </div>
                <div class="news-stack">
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                </div>
                <div class="news-stack">
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                    <div class="mobile-news">
                        <div class="image" style="background-image: url('<?= get_template_directory_uri() ?>/img/news-baner.png')">
                            <h2>новогодний корпоратив в стиле алиса в стране чудес</h2>
                            <a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="news-text">
                            <p>30 декабря 2016</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque aut dolore doloribus dolorum explicabo illo in incidunt inventore iste iusto, modi odio officia officiis porro quam qui sunt vero!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

<?php get_template_part('template-parts/reviews_block'); ?>

    <section class="invite-to-lesson mobile-hidden">
        <div class="wrapper">
            <div class="form-invite">
                <h1>записаться на пробное занятие</h1>
                <form action="" method="post" class="invite-to-course">
                    <div class="input-container">
                        <input type="text" name="name" placeholder="ИМЯ*">
                        <p class="error-message">Введите имя</p>
                    </div>
                    <div class="input-container">
                        <input type="text" name="phone" placeholder="ТЕЛЕФОН*">
                        <p class="error-message">Введите телефон</p>
                    </div>
                    <div class="input-container">
                        <input type="email" name="email" placeholder="E-MAIL*">
                        <p class="error-message">Введите email</p>
                    </div>
                    <div class="input-container">
                        <button class="submit-btn" type="submit">записаться</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

<?php

get_footer();
