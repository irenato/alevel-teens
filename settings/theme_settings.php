<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 25.02.17
 * Time: 15:04
 */


function theme_settings_page()
{

    echo '<div class="wrap">';
    echo '<h1>Настройки темы</h1>';
    echo '<form method="post" action="options.php" enctype="multipart/form-data">';
    settings_fields("section");
    do_settings_sections("theme-options");
    submit_button();
    echo '</form>';
    echo '</div>';
}

function display_phone1_element()
{
    echo '<input type="text" name="phone1" id="phone1" value="' . get_option('phone1') . '"/>';
}

function display_phone2_element()
{
    echo '<input type="text" name="phone2" id="phone2" value="' . get_option('phone2') . '"/>';
}

function display_admin_email_element()
{
    echo '<input type="text" name="admin_email" id="admin_email" value="' . get_option('admin_email') . '"/>';
}

function display_city_element()
{
    echo '<input type="text" name="city" id="city" value="' . get_option('city') . '"/>';
}


function display_address_element()
{
    echo '<input type="text" name="address" id="address" value="' . get_option('address') . '"/>';
}

function display_coordinates_element()
{
    echo '<input type="text" name="coordinates" id="coordinates" value="' . get_option('coordinates') . '"/>';
}

function display_googlemaps_element()
{
    echo '<input type="text" name="googlemaps" id="googlemaps" value="' . get_option('googlemaps') . '"/>';
}

function display_vkontakte_element()
{
    echo '<input type="text" name="vkontakte_url" id="vkontakte_url" value="' . get_option('vkontakte_url') . '"/>';
}

function display_facebook_element()
{
    echo '<input type="text" name="facebook_url" id="facebook_url" value="' . get_option('facebook_url') . '"/>';
}

function display_googleplus_element()
{
    echo '<input type="text" name="googleplus_url" id="googleplus_url" value="' . get_option('googleplus_url') . '"/>';
}

function display_pagetitle_element()
{
    echo '<input type="text" name="alevel_pagetitle" id="alevel_pagetitle" value="' . get_option('alevel_pagetitle') . '"/>';
}

function display_pagetitle_description_element()
{
    echo '<textarea name="alevel_pagetitle_description" id="alevel_pagetitle_description">' . get_option('alevel_pagetitle_description') . '</textarea>';
}

function logo_display()
{
    echo '<input type="file" name="logo"/>';
    echo get_option('logo');
}

function handle_logo_upload()
{

    if (!empty($_FILES["logo"]["tmp_name"])) {
        $urls = wp_handle_upload($_FILES["logo"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    }

    return get_option('logo');
}

//function auditory_display()
//{
//    echo '<input type="file" name="auditory"/>';
//    echo get_option('auditory');
//}
//
//function auditory_upload()
//{
//    global $option;
//
//    if (!empty($_FILES["auditory"]["tmp_name"])) {
//        $urls = wp_handle_upload($_FILES["auditory"], array('test_form' => FALSE));
//        $temp = $urls["url"];
//        return $temp;
//    }
//
//    return $option;
//}

function display_theme_panel_fields()
{
    add_settings_section("section", "", null, "theme-options");
    add_settings_field("logo", "Логотип", "logo_display", "theme-options", "section");
    add_settings_field("alevel_pagetitle", "Заголовок", "display_pagetitle_element", "theme-options", "section");
    add_settings_field("alevel_pagetitle_description", "Заголовок(описание)", "display_pagetitle_description_element", "theme-options", "section");
    add_settings_field("phone1", "Контактный номер телефона", "display_phone1_element", "theme-options", "section");
    add_settings_field("phone2", "Контактный номер телефона", "display_phone2_element", "theme-options", "section");
    add_settings_field("admin_email", "E-mail основной", "display_admin_email_element", "theme-options", "section");
    add_settings_field("city", "Город", "display_city_element", "theme-options", "section");
    add_settings_field("address", "Адрес", "display_address_element", "theme-options", "section");
    add_settings_field("coordinates", "Координаты", "display_coordinates_element", "theme-options", "section");
    add_settings_field("googlemaps", "Ссылка на google maps", "display_googlemaps_element", "theme-options", "section");
    add_settings_field("vkontakte_url", "Ссылка на группу Вконтакте", "display_vkontakte_element", "theme-options", "section");
    add_settings_field("facebook_url", "Ссылка на групу в Facebook", "display_facebook_element", "theme-options", "section");
    add_settings_field("googleplus_url", "Ссылка на групу в G+", "display_googleplus_element", "theme-options", "section");

    register_setting("section", "logo", "handle_logo_upload");
    register_setting("section", "alevel_pagetitle");
    register_setting("section", "alevel_pagetitle_description");
    register_setting("section", "phone1");
    register_setting("section", "phone2");
    register_setting("section", "admin_email");
    register_setting("section", "city");
    register_setting("section", "address");
    register_setting("section", "coordinates");
    register_setting("section", "googlemaps");
    register_setting("section", "vkontakte_url");
    register_setting("section", "facebook_url");
    register_setting("section", "googleplus_url");
}

add_action("admin_init", "display_theme_panel_fields");

function add_theme_menu_item()
{
    add_menu_page("Контактные данные", "Контактные данные", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");
