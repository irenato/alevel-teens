<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 04.03.17
 * Time: 23:07
 */

function add_theme_menu_advers()
{
    add_menu_page("Заявки (студенты)", "Заявки (студенты)", "manage_options", "theme-panel2", "selectAllApplicationsFromStudents", 'dashicons-carrot', 4);
}


add_action("admin_menu", "add_theme_menu_advers");

function countNewApplications()
{
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_alevel_courses_students WHERE status=0;");
    if ($count_applications)
        return $count_applications;
    else
        return 0;
}

function register_my_custom_submenu_page()
{
    add_submenu_page('theme-panel2', 'Новые заявки', 'Новые заявки (' . countNewApplications() . ')', 'manage_options', 'my-custom-submenu-page1', 'selectNewApplicationsFromStudents');
}

add_action('admin_menu', 'register_my_custom_submenu_page');

function register_my_custom_submenu_page2()
{
    add_submenu_page('theme-panel2', 'Обработанные заявки', 'Обработанные заявки', 'manage_options', 'my-custom-submenu-page2', 'selectConfirmedApplicationsFromStudents');
}

add_action('admin_menu', 'register_my_custom_submenu_page2');



function add_theme_menu_advers2()
{
    add_menu_page("Заявки (учителя)", "Заявки (учителя)", "manage_options", "theme-panel4", "selectAllApplicationsFromTeachers", 'dashicons-smiley', 4);
}


add_action("admin_menu", "add_theme_menu_advers2");

function countNewApplicationsTeachers()
{
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_alevel_courses_teachers WHERE status=0;");
    if ($count_applications)
        return $count_applications;
    else
        return 0;
}

function register_my_custom_submenu_page3()
{
    add_submenu_page('theme-panel4', 'Новые заявки', 'Новые заявки (' . countNewApplicationsTeachers() . ')', 'manage_options', 'my-custom-submenu-page3', 'selectNewApplicationsFromTeachers');
}

add_action('admin_menu', 'register_my_custom_submenu_page3');

function register_my_custom_submenu_page4()
{
    add_submenu_page('theme-panel4', 'Обработанные заявки', 'Обработанные заявки', 'manage_options', 'my-custom-submenu-page4', 'selectConfirmedApplicationsFromTeachers');
}

add_action('admin_menu', 'register_my_custom_submenu_page4');