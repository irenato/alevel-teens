<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 12:28
 */

//------------------------------------------
// курсы
//------------------------------------------
add_action('init', 'sections_register1');
function sections_register1()
{
    register_post_type('courses',
        array(
            'label' => __('Курсы'),
            'singular_label' => 'course',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-format-aside',
            'supports' => array(
                'title',
                'thumbnail',
                'editor',
            ),
        )
    );
}

//------------------------------------------
// преподователи
//------------------------------------------
add_action('init', 'sections_register');
function sections_register()
{
    register_post_type('teachers',
        array(
            'label' => __('Учителя'),
            'singular_label' => 'teacher',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-admin-users',
            'supports' => array(
                'title',
                'thumbnail',
                'editor',
            ),
        )
    );
}

//------------------------------------------
// связывание курс - учитель
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post()
    {
        p2p_register_connection_type(array(
            'name' => 'courses_to_teachers',
            'from' => 'courses',
            'to' => 'teachers'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post');
}

//------------------------------------------
// отзывы
//------------------------------------------
add_action('init', 'register_reviews');
function register_reviews()
{
    register_post_type('reviews',
        array(
            'label' => __('Отзывы'),
            'singular_label' => 'review ',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-format-status',
            'supports' => array(
                'title',
                'thumbnail',
            ),
        )
    );
}

