<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 04.03.17
 * Time: 18:51
 */

add_action('wp_ajax_nopriv_inviteCourse', 'inviteCourse');
add_action('wp_ajax_inviteCourse', 'inviteCourse');

function inviteCourse()
{
    global $wpdb;
    $data = array();
    parse_str($_POST['user'], $data);
    $fields = array(
        'course_id' => (int)$data['course'],
        'username' => $data['name'],
        'email' => $data['email'],
        'phone' => $data['phone'],
        'created_at' => time(),
        'status' => 0,
    );
    $headers = 'From: ' . $data['email'] . '.' . '<' . $data['email'] . '>';
    $to = get_option('admin_email');
    $mail_subject = 'Новая заявка (студент) A-level-teens ' . $data['name'] . "\r\n";
    $mail_text = 'Название курса :' . get_the_title((int)$data['course']) . "\r\n";
    $mail_text .= 'Контактные данные ' . $data['name'] . "\r\n";
    $mail_text .= 'Электронная почта: ' . $data['email'] . "\r\n";
    $mail_text .= 'Контактный телефон: ' . $data['phone'] . "\r\n";
    wp_mail($to, $mail_subject, $mail_text, $headers);
    echo $wpdb->insert('wp_alevel_courses_students', $fields, '');
    die();
}

add_action('wp_ajax_nopriv_newTeacher', 'newTeacher');
add_action('wp_ajax_newTeacher', 'newTeacher');

function newTeacher()
{
    global $wpdb;
    $data = array();
    parse_str($_POST['user'], $data);
    $fields = array(
        'course' => $data['course'],
        'username' => $data['first-name'],
        'username2' => $data['last-name'],
        'email' => $data['email'],
        'phone' => $data['phone'],
        'created_at' => time(),
        'status' => 0,
    );
    $headers = 'From: ' . $data['email'] . '.' . '<' . $data['email'] . '>';
    $to = get_option('admin_email');
    $mail_subject = 'Новая заявка (преподаватель) A-level-teens ' . $data['name'] . "\r\n";
    $mail_text = 'Название курса :' . $data['course'] . "\r\n";
    $mail_text .= 'Контактные данные ' . $data['name'] . "\r\n";
    $mail_text .= 'Электронная почта: ' . $data['email'] . "\r\n";
    $mail_text .= 'Контактный телефон: ' . $data['phone'] . "\r\n";
    wp_mail($to, $mail_subject, $mail_text, $headers);
    echo $wpdb->insert('wp_alevel_courses_teachers', $fields, '');
    die();
}


add_action('wp_ajax_nopriv_addReview', 'addReview');
add_action('wp_ajax_addReview', 'addReview');

function addReview()
{
    global $wpdb;
    $data = array();
    parse_str($_POST['review'], $data);
    $name = stripcslashes(trim($data['first-name'])) . ' ' . stripcslashes(trim($data['last-name']));
    $email = stripcslashes(trim($data['email']));
    $review_course = stripcslashes(trim($data['course']));
    $review = stripcslashes(trim(trim($data['review'])));
    $fields = array( // подготовим массив с полями поста, ключ это название поля, значение - его значение
        'post_type' => 'reviews', // нужно указать какой тип постов добавляем, у нас это my_custom_post_type
        'post_title' => $name, // заголовок поста
        'post_status' => 'pending',
    );
    $post_id = wp_insert_post($fields); // добавляем пост в базу и получаем его id
    if ($_FILES[0]) { // если основное фото было загружено
        $attach_id_img = media_handle_upload('0', $post_id); // добавляем картинку в медиабиблиотеку и получаем её id
        update_post_meta($post_id, '_thumbnail_id', $attach_id_img); // привязываем миниатюру к посту
    }
    if (update_post_meta($post_id, 'review', $review) && update_post_meta($post_id, 'review_email', $email) && update_post_meta($post_id, 'course_name', $review_course)) {
        print('done!');
        die();
    }
}

add_action('wp_ajax_nopriv_confirmApplicationFromStudents', 'confirmApplicationFromStudents');
add_action('wp_ajax_confirmApplicationFromStudents', 'confirmApplicationFromStudents');

function confirmApplicationFromStudents()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_alevel_courses_students';
    $field['status'] = 1;
    $where['id'] = $id;
    return $wpdb->update($table, $field, $where);
    die();
}

add_action('wp_ajax_nopriv_deleteApplicationFromStudents', 'deleteApplicationFromStudents');
add_action('wp_ajax_deleteApplicationFromStudents', 'deleteApplicationFromStudents');

function deleteApplicationFromStudents()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_alevel_courses_students';
    $where['id'] = $id;
    return $wpdb->delete($table, $where);
    die();
}


add_action('wp_ajax_nopriv_confirmApplicationFromTeachers', 'confirmApplicationFromTeachers');
add_action('wp_ajax_confirmApplicationFromTeachers', 'confirmApplicationFromTeachers');

function confirmApplicationFromTeachers()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_alevel_courses_teachers';
    $field['status'] = 1;
    $where['id'] = $id;
    return $wpdb->update($table, $field, $where);
    die();
}

add_action('wp_ajax_nopriv_deleteApplicationFromTeachers', 'deleteApplicationFromTeachers');
add_action('wp_ajax_deleteApplicationFromTeachers', 'deleteApplicationFromTeachers');

function deleteApplicationFromTeachers()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_alevel_courses_teachers';
    $where['id'] = $id;
    return $wpdb->delete($table, $where);
    die();
}