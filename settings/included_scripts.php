<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 25.02.17
 * Time: 15:07
 */

function a_level_styles()
{
    wp_enqueue_style('a_level-slick-theme', get_template_directory_uri() . '/libs/slick/slick-theme.css', '', '', 'all');
    wp_enqueue_style('a_level-slick', get_template_directory_uri() . '/libs/slick/slick.css', '', '', 'all');
    wp_enqueue_style('a_level-font-awesome', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
    wp_enqueue_style('a_level-fonts', get_template_directory_uri() . '/css/fonts.min.css', '', '', 'all');
    wp_enqueue_style('a_level-header', get_template_directory_uri() . '/css/header.min.css', '', '', 'all');
    wp_enqueue_style('a_level-lvl', get_template_directory_uri() . '/css/lvl.css', '', '', 'all');
    wp_enqueue_style('a_level-smart', get_template_directory_uri() . '/css/smart-grid.min.css', '', '', 'all');
    wp_enqueue_style('a_level-main', get_template_directory_uri() . '/css/main.min.css', '', '', 'all');
}

add_action('wp_enqueue_scripts', 'a_level_styles');

function a_level_scripts()
{
    wp_enqueue_script('a_level-libs', get_template_directory_uri() . '/js/libs.js', false, '', true);
    wp_enqueue_script('a_level-slick', get_template_directory_uri() . '/libs/slick/slick.min.js', false, '', true);
    wp_enqueue_script('a_level-maskedinput', get_template_directory_uri() . '/js/jquery.maskedinput-1.3.js', false, '', true);
    wp_enqueue_script('a_level-common', get_template_directory_uri() . '/js/common.js', false, '', true);
    wp_enqueue_script('a_level-action', get_template_directory_uri() . '/js/action.js', false, '', true);
    wp_localize_script( 'a_level-action', 'alevel_ajax', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}

add_action('wp_enqueue_scripts', 'a_level_scripts');



function scriptForAdmin()
{
    wp_enqueue_script('admin_action', get_template_directory_uri() . '/js/admin_action.js', array(), '', true);
}

add_action('admin_enqueue_scripts', 'scriptForAdmin');

