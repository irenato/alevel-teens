<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 04.03.17
 * Time: 23:37
 */

function selectAllApplicationsFromTeachers()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `username`, `username2`, `phone`, `email`, `phone`, `course`, `created_at`, `status` FROM wp_alevel_courses_teachers ORDER BY `id` DESC ");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                фамилия
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                статус заявки
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username2; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->email; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->course ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->created_at); ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->status == 0 ? 'new' : 'confirmed'; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}

function selectNewApplicationsFromTeachers()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `username`, `username2`, `phone`, `email`, `course`, `created_at`, `status` FROM `wp_alevel_courses_teachers`  WHERE `status`=0 ORDER BY `id` DESC");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                фамилия
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                действие
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username2; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->email; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->course ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->date); ?>
                    </td>
                    <td class="manage-column">
                        <a class="confirm-teacher" data-id="<?= $application->id; ?>" href="#">подтвердить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}

function selectConfirmedApplicationsFromTeachers()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `username`, `username2`, `phone`, `email`, `phone`, `course`, `created_at`, `status` FROM wp_alevel_courses_teachers  WHERE `status`=1 ORDER BY `id` DESC");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                фамилия
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                действие
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username2; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->email; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->course ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->date); ?>
                    </td>
                    <td class="manage-column">
                        <a class="remove-teacher" data-id="<?= $application->id; ?>" href="#">удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}