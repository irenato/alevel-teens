<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 04.03.17
 * Time: 23:10
 */

function selectAllApplicationsFromStudents()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `username`, `phone`, `email`, `phone`, `course_id`, `created_at`, `status` FROM wp_alevel_courses_students ORDER BY `id` DESC ");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                статус заявки
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->email; ?>
                    </td>
                    <td class="manage-column">
                        <?php if ($application->course_id == 0) : ?>
                            пробный
                        <?php elseif ($application->course_id == 1) : ?>
                            заказали звонок
                        <?php else : ?>
                            <?= get_the_title($application->course_id) ?>
                        <?php endif; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->created_at); ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->status == 0 ? 'new' : 'confirmed'; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}

function selectNewApplicationsFromStudents()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `username`, `phone`, `email`, `course_id`, `created_at`, `status` FROM `wp_alevel_courses_students`  WHERE `status`=0 ORDER BY `id` DESC");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                действие
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->email; ?>
                    </td>
                    <td class="manage-column">
                        <?php if ($application->course_id == 0) : ?>
                            пробный
                        <?php elseif ($application->course_id == 1) : ?>
                            заказали звонок
                        <?php else : ?>
                            <?= get_the_title($application->course_id) ?>
                        <?php endif; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->date); ?>
                    </td>
                    <td class="manage-column">
                        <a class="confirm-it-now" data-id="<?= $application->id; ?>" href="#">подтвердить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}

function selectConfirmedApplicationsFromStudents()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `username`, `phone`, `email`, `phone`, `course_id`, `created_at`, `status` FROM wp_alevel_courses_students  WHERE `status`=1 ORDER BY `id` DESC");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                действие
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->email; ?>
                    </td>
                    <td class="manage-column">
                        <?php if ($application->course_id == 0) : ?>
                            пробный
                        <?php elseif ($application->course_id == 1) : ?>
                            заказали звонок
                        <?php else : ?>
                            <?= get_the_title($application->course_id) ?>
                        <?php endif; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->date); ?>
                    </td>
                    <td class="manage-column">
                        <a class="delete-now" data-id="<?= $application->id; ?>" href="#">удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}