<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 26.02.17
 * Time: 17:21
 */

$months = array(
    1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля',
    5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа',
    9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря'
);

function createDate($course_date){
    global $months;
    $date = explode('.', $course_date);
    return $date[0] . ' ' . $months[(int)$date[1]];
}