<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 04.03.17
 * Time: 22:25
 */

?>

<section class="feed-back" id="comments">
    <h2 class="double-title">отзывы</h2>
    <div class="bg-fb">
        <div class="wrapper">
            <div class="carusel" id="carusel">
                <?php $args = array(
                    'offset' => 0,
                    'post_type' => 'reviews',
                    'posts_per_page' => -1); ?>
                <?php $reviews = new WP_query($args); ?>
                <?php while ($reviews->have_posts()) : $reviews->the_post(); ?>
                    <div class="carusel-content">
                        <div class="person">
                            <div class="img" style="background: url(<?= get_the_post_thumbnail_url() ?>) center center no-repeat;">
                                <img src="<?= get_the_post_thumbnail_url() ?>"
                                     alt="<?= get_the_title() ?>">
                            </div>
                            <p>
                                <?= get_the_title() ?>
                            </p>
                        </div>
                        <div class="person-comment-block">
                            <p>
                                <?= get_field('review') ?>
                            </p>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>                
            </div>
            <button class="add-comment" id="add_comment">добавить отзыв</button>
        </div>
</section>
