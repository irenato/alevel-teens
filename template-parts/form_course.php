<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 04.03.17
 * Time: 17:31
 */

?>

<section class="invite-to-course-sc" id="invite-to-course">
    <div class="wrapper">
        <div class="row">
            <h2 class="double-title">записаться на курс</h2>
            <form action="" method="post" class="invite-to-course-m invite-to-course">
                <div class="inputs">
                    <div class="input-container">
                        <input type="text" name="name" placeholder="Имя и Фамилия">
                        <p class="error-message">Введите имя и фамилию</p>
                    </div>
                    <div class="input-container">
                        <select name="course">
                            <?php $args = array(
                                'offset' => 0,
                                'post_type' => 'courses',
                                'posts_per_page' => -1); ?>
                            <?php $post_courses = new WP_query($args); ?>
                            <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>

                                <option value="<?= get_the_ID() ?>"><?= get_the_title() ?></option>

                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                        </select>
                        <p class="error-message">Выберите курс</p>
                    </div>
                    <div class="input-container">
                        <input type="text" name="phone" placeholder="Телефон">
                        <p class="error-message">Телефон введен неверно</p>
                    </div>
                    <div class="input-container">
                        <input type="text" name="email" placeholder="E-Mail">
                        <p class="error-message">Email введен не верно</p>
                    </div>
                </div>
                <button type="submit" class="submit-invite-form">ЗАПИСАТЬСЯ</button>
                <p>
                    *Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае
                    не будут переданы другим пользователям и никогда не будут открыты для просмотра.
                    Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные
                    третьим лицам.
                </p>
            </form>
        </div>
    </div>
</section>
